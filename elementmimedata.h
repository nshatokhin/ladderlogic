#ifndef ELEMENTMIMEDATA_H
#define ELEMENTMIMEDATA_H

#include <QMimeData>

// mimedata needed for drag & drop. We will get data from it, when user drops item. We will detect item by it and get params from it

class ElementMimeData : public QMimeData
{
public:
    ElementMimeData();

    // possible types of element
    enum Types {INSTRUCTION, TOOL};

    // getter & setter
    Types type();
    void setType(Types type);

protected:
    // type of element
    Types _type;

};

#endif // ELEMENTMIMEDATA_H
