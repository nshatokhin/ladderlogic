#ifndef RUNG_H
#define RUNG_H

#include <QDebug>
#include <QObject>
#include <QPainter>
#include <QPixmap>

#include "basicinstruction.h"
#include "element.h"
#include "instruction.h"
#include "line.h"
#include "node.h"
#include "tools.h"

#define DEFAULT_RUNG_HEIGHT 70
#define DEFAULT_LINE_HEIGHT 60
#define DEFAULT_LINE_WIDTH 60
#define DEFAULT_RUNG_VERTICAL_PADDING 10

class Rung : public QObject
{
    Q_OBJECT
public:
    explicit Rung(int x, int y, int width, QObject *parent = 0);
    Rung(QDataStream &stream);
    ~Rung();

    int x();
    int y();
    void move(int x, int y);
    int width();
    int height();
    void resize(int width, int height);

    int startNodeNumber();
    void setStartNodeNumber(int number);
    int maximumNodeNumber();

    bool contains(int x, int y);
    void mousePressed(int x, int y);
    void mouseReleased(int x, int y);
    void mouseMoving(int x, int y);

    QList<Line *> lines();
    QList<Node *> nodes();
    QList<Element *> elements();

    QString toXml();

    void load(QDataStream &stream);
    void save(QDataStream &stream);
    
signals:
    void resized();
    void nodeAdded();
    
public slots:
    void addElement(int x, int y, int frameX, int frameY, int frameWidth, int frameHeight, Instruction *instruction);
    void addLine(int x, int y, int frameX, int frameY, int frameWidth, int frameHeight);
    void draw(QPainter &painter, int frameX, int frameY, int frameWidth, int frameHeight);
    void drawNodes(QPainter &painter, int frameX, int frameY, int frameWidth, int frameHeight);

protected:
    int rungHeight;
    int lineHeight;
    int lineWidth;
    int rungVerticalPadding;

    int _x, _y;
    int _width, _height;

    int _startNodeNumber;

    QList<Line *> _lines;
    QList<Node *> _nodes;
    QList<Element *> _elements;

    // TEMP
    Line * _activeLine;
    bool _movingLineLeftBorder;
    bool _movingLineRightBorder;
    Node * _movingNode;
    int _defaultLine;

    int addNode(Node *node);
    Node * addNodeToLine(int x, Line * line);
    Node * takeNode(int node);
    Node * getNode(int node);
    int findRightNode(int x, int y, QList<Node *> nodes);
    int findRightElement(int x, int y, QList<Element *> &elements);
    void reconnectNodes();
};

#endif // RUNG_H
