#include "line.h"

Line::Line(int x, int y, int width, int height, Node *inputNode, Node *outputNode, Line *parentLineLeft, Line *parentLineRight, int index, QObject *parent) :
    QObject(parent)
{
    _x = x;
    _y = y;
    _width = width;
    _height = height;
    _index = index;
    _inputNode = inputNode;
    _outputNode = outputNode;
    _parentLineLeft = parentLineLeft;
    _parentLineRight = parentLineRight;

    clearNodes();
}

Line::~Line()
{
}

int Line::x()
{
    return _x;
}

int Line::y()
{
    return _y;
}

int Line::width()
{
    return _width;
}

int Line::height()
{
    return _height;
}

int Line::index()
{
    return _index;
}

void Line::setIndex(int index)
{
    _index = index;
}

Node * Line::inputNode()
{
    return _inputNode;
}

void Line::setInputNode(Node * node)
{
    _inputNode = node;
}

Node * Line::outputNode()
{
    return _outputNode;
}

void Line::setOutputNode(Node * node)
{
    _outputNode = node;
}

Line *Line::parentLineLeft()
{
    return _parentLineLeft;
}

void Line::setParentLineLeft(Line *line)
{
    _parentLineLeft = line;
}

Line *Line::parentLineRight()
{
    return _parentLineRight;
}

void Line::setParentLineRight(Line *line)
{
    _parentLineRight = line;
}


bool Line::contains(int x, int y)
{
    // true, if point located between top left corner and bottom right
    return ((x >= _x) && (y >= _y) &&
            (x < _x + _width) && (y < _y + _height));
}

bool Line::intersection(Line * line)
{
    return ((line->x()+line->width()-1>=x()) && (line->x()<=x()+width()-1) &&
            (line->y()+line->height()-1>=y()) && (line->y()<=y()+height()-1));
}

void Line::move(int x, int y)
{
    _x = x;
    _y = y;
}

void Line::resize(int width, int height)
{
    _width = width;
    _height = height;
}

QList<Element *> Line::elements()
{
    return _elements;
}

void Line::addElement(Element *element)
{
    _elements.push_back(element);
}

QList<Node *> Line::nodes()
{
    return _nodes;
}

void Line::addNode(Node *node)
{
    if(!_nodes.contains(node))
    {
        for(int i=0;i<_nodes.size();i++)
        {
            if(_nodes[i]->index() > node->index())
            {
                _nodes.insert(i, node);
                return;
            }
        }

        _nodes.push_back(node);
    }
}

void Line::clearNodes()
{
    _nodes.clear();
    _nodes.push_back(_inputNode);
    _nodes.push_back(_outputNode);
}

Node *Line::findLeftNode(int x, int)
{
    for(int i=0;i<_nodes.size();i++)
    {
        if(_nodes[i]->x() > x)
        {
            if(i > 0)
                return _nodes[i-1];
            else
                return NULL;
        }
    }

    return NULL;
}

Node *Line::findRightNode(int x, int)
{
    for(int i=0;i<_nodes.size();i++)
    {
        if(x < _nodes[i]->x())
            return _nodes[i];
    }

    return NULL;
}

Element *Line::findRightElement(int x, int)
{
    for(int i=0;i<_elements.size();i++)
    {
        if(x < _elements[i]->x())
            return _elements[i];
    }

    return NULL;
}

QList<Element *> Line::findElementsBetweenTwoPoints(int x1, int, int x2, int)
{
    QList<Element *> elements;

    for(int i=0;i<_elements.size();i++)
    {
        if(_elements[i]->x() >= x1 && _elements[i]->x() < x2)
            elements.push_back(_elements[i]);
    }

    return elements;
}
