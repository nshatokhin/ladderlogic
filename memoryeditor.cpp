#include "memoryeditor.h"
#include "ui_memoryeditor.h"

MemoryEditor::MemoryEditor(QWidget *parent) :
    QWidget(parent, Qt::Window),
    ui(new Ui::MemoryEditor)
{
    ui->setupUi(this);

    // making table's labels list
    QStringList labels;
    labels.push_back(this->tr("in_slot"));
    labels.push_back(this->tr("alias"));
    labels.push_back(this->tr("value"));
    // set labels to table columns
    ui->tableWidget->setHorizontalHeaderLabels(labels);
    //signal for changed selecting
    connect(ui->tableWidget, SIGNAL(itemSelectionChanged()), this, SLOT(selectionChanged()));
    // accept & reject dialog signals
    connect(ui->okButton, SIGNAL(clicked()), this, SLOT(accept()));
    connect(ui->cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

    // disabling some buttons that must be active when selection exists
    ui->addBeforeButton->setEnabled(false);
    ui->addAfterButton->setEnabled(false);
    ui->deleteButton->setEnabled(false);

    // signals of add/delete rows buttons
    connect(ui->addEndButton, SIGNAL(clicked()), this, SLOT(addToEnd()));
    connect(ui->addBeforeButton, SIGNAL(clicked()), this, SLOT(addBefore()));
    connect(ui->addAfterButton, SIGNAL(clicked()), this, SLOT(addAfter()));
    connect(ui->deleteButton, SIGNAL(clicked()), this, SLOT(deleteRows()));
}

MemoryEditor::~MemoryEditor()
{
    delete ui;
}

void MemoryEditor::setMemoryTable(MemoryTable *table)
{
    _table = table; // set data table to editor
    // clear table widget in editor
    while(ui->tableWidget->rowCount() > 0)
        ui->tableWidget->removeRow(0);

    // and fill it by records from data table
    for(int i=0;i<_table->records().size();i++)
    {
        const int currentRow = ui->tableWidget->rowCount();
        ui->tableWidget->insertRow(currentRow);   // add new row to end of table
        // and write data to cells of this row
        ui->tableWidget->setItem(currentRow, 0, new QTableWidgetItem(_table->records()[i].inSlot()));
        ui->tableWidget->setItem(currentRow, 1, new QTableWidgetItem(_table->records()[i].alias()));
        ui->tableWidget->setItem(currentRow, 2, new QTableWidgetItem(_table->records()[i].value()));
    }
}

void MemoryEditor::accept() // dialog was accepted
{
    updateTable();  // update records in data table
    hide();     // close editor
}

void MemoryEditor::reject() // dialog was rejected
{
    hide();     //close editor
}

void MemoryEditor::selectionChanged()
{
    qDebug() << "selection changed";

    QModelIndexList selected = ui->tableWidget->selectionModel()->selectedRows();   // get selected rows

    if(selected.size() > 0) // if selected rows exist
    {
        // enable selection depended buttons
        ui->addBeforeButton->setEnabled(true);
        ui->addAfterButton->setEnabled(true);
        ui->deleteButton->setEnabled(true);
    }
    else
    {
        // else disable selection depended button
        ui->addBeforeButton->setEnabled(false);
        ui->addAfterButton->setEnabled(false);
        ui->deleteButton->setEnabled(false);
    }
}

void MemoryEditor::addBefore()
{
    QModelIndexList selected = ui->tableWidget->selectionModel()->selectedRows();   // get selected rows

    int row = selected[0].row();    // position - before selection

    ui->tableWidget->insertRow(row);    // add row to position
}

void MemoryEditor::addAfter()
{
    QModelIndexList selected = ui->tableWidget->selectionModel()->selectedRows();   // get selected rows

    int row = selected[selected.length()-1].row();  // position - last selected row

    ui->tableWidget->insertRow(row + 1);    // add new row after last selected
}

void MemoryEditor::addToEnd()
{
    ui->tableWidget->insertRow(ui->tableWidget->rowCount());    // add row to end of table
}

void MemoryEditor::deleteRows()
{
    QModelIndexList selected = ui->tableWidget->selectionModel()->selectedRows();   // get selected rows

    for(int i=selected.size()-1;i>=0;i--)   // for each selected row
    {
        ui->tableWidget->removeRow(selected[i].row());  // delete this row
    }
}

void MemoryEditor::updateTable()    // update data in data table
{
    _table->clear();    // clear data table

    for(int i=0;i<ui->tableWidget->rowCount();i++)  // for each row in table widget
    {
        // get record params
        QString inSlot, alias, value;
        if(ui->tableWidget->item(i, 0) != NULL)
            inSlot = ui->tableWidget->item(i, 0)->text().trimmed();
        if(ui->tableWidget->item(i, 1) != NULL)
            alias = ui->tableWidget->item(i, 1)->text().trimmed();
        if(ui->tableWidget->item(i, 2) != NULL)
            value = ui->tableWidget->item(i, 2)->text().trimmed();

        // if any record param is empty - skip this record
        if(inSlot.isEmpty() || alias.isEmpty() || value.isEmpty()) continue;
        // else add record to data table
        _table->addRecord(MemoryRecord(inSlot, alias, value));
    }
}
