#-------------------------------------------------
#
# Project created by QtCreator 2014-01-16T19:58:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LadderLogic
TEMPLATE = app


SOURCES += main.cpp\
        logiceditor.cpp \
    draggableelement.cpp \
    element.cpp \
    componentpropertiesdialog.cpp \
    node.cpp \
    line.cpp \
    rung.cpp \
    drawarea.cpp \
    memoryeditor.cpp \
    instruction.cpp \
    basicinstruction.cpp \
    memoryrecord.cpp \
    memorytable.cpp \
    instructionmimedata.cpp \
    tools.cpp \
    timerinstruction.cpp \
    comparatorinstruction.cpp \
    mathinstruction.cpp \
    draggableinstructionelement.cpp \
    elementmimedata.cpp \
    toolmimedata.cpp \
    draggabletoolelement.cpp

HEADERS  += logiceditor.h \
    draggableelement.h \
    element.h \
    componentpropertiesdialog.h \
    node.h \
    line.h \
    drawarea.h \
    rung.h \
    memoryeditor.h \
    instruction.h \
    basicinstruction.h \
    memoryrecord.h \
    memorytable.h \
    instructionmimedata.h \
    tools.h \
    timerinstruction.h \
    comparatorinstruction.h \
    mathinstruction.h \
    draggableinstructionelement.h \
    elementmimedata.h \
    toolmimedata.h \
    draggabletoolelement.h

FORMS    += logiceditor.ui \
    componentpropertiesdialog.ui \
    memoryeditor.ui

RESOURCES += \
    resources.qrc
