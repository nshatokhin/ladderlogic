#ifndef TOOLMIMEDATA_H
#define TOOLMIMEDATA_H

#include "elementmimedata.h"

// mimedata needed for drag & drop. We will get data from it, when user drops item. We will detect item by it and get params from it

class ToolMimeData : public ElementMimeData
{
public:
    ToolMimeData();

    // possible types of tool elements
    enum Tools {LINE};

    // getter and setter
    Tools tool();
    void setTool(Tools tool);

protected:
    Tools _tool; // type of tool element

};

#endif // TOOLMIMEDATA_H
