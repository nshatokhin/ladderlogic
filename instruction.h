#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include <QObject>
#include <QPixmap>

class Instruction : public QObject
{
    Q_OBJECT
public:
    explicit Instruction(QObject *parent = 0);
    explicit Instruction(QString opcode, QString alias, QString imagePath, QObject *parent = 0);
    virtual ~Instruction();

    // possible kinds of instructions
    enum Type {BASIC, TIMER, COMPARATOR, MATH};

    // instruction params
    Type type() const;
    QString opcode() const;
    QString alias() const;
    QPixmap image() const;
    QString imagePath() const;

    // params setters
    void setOpcode(QString opcode);
    void setAlias(QString alias);
    void setImage(QPixmap image);
    void setImagePath(QString imagePath);

    // pure virtual function for cloning instruction
    virtual Instruction * clone() = 0;

    // save & load functions
    virtual void save(QDataStream &stream);
    virtual void load(QDataStream &stream);
    
signals:
    
public slots:

protected:
    // params
    Type _type;
    QString _opcode;
    QString _alias;
    QString _imagePath;
    QPixmap _image;
    
};

#endif // INSTRUCTION_H
