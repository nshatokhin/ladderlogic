#ifndef LOGICEDITOR_H
#define LOGICEDITOR_H

#include <QFileDialog>
#include <QMainWindow>

#include "componentpropertiesdialog.h"
#include "draggableelement.h"
#include "element.h"
#include "memoryeditor.h"
#include "memorytable.h"
#include "tools.h"

namespace Ui {
class LogicEditor;
}

class LogicEditor : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit LogicEditor(QWidget *parent = 0);
    ~LogicEditor();
    
private:
    Ui::LogicEditor *ui;

signals:
    void quit();    // exit from application

protected:
    ComponentPropertiesDialog * componentPropertiesDialog; // dialog for editing element properties
    MemoryEditor * memoryEditor;    // dialog for editing data table

    // data tables
    MemoryTable * inputsTable;
    MemoryTable * outputsTable;
    MemoryTable * ramTable;
    MemoryTable * romTable;

    // closing window event
    void closeEvent(QCloseEvent *);

protected slots:
    // editing properties of element
    void changeComponentProperties(Element * element);

    // show one of data tables
    void showInputsTable();
    void showOutputsTable();
    void showRamTable();
    void showRomTable();

    void clear();   // set application state to initial
    void save();    // save diagram
    void load();    // load diagram from file
    void exportXml();   // export diagram to XML
    void exit();    // exit from application
};

#endif // LOGICEDITOR_H
