#ifndef ELEMENT_H
#define ELEMENT_H

#include <QObject>
#include <QPixmap>

#include "instruction.h"
#include "node.h"

class Node;

class Element : public QObject
{
    Q_OBJECT
public:
    explicit Element(QObject *parent = 0);
    Element(int x, int y, int width, int height, Node * left, Node * right, int line, Instruction * instruction, QObject *parent = 0);
    ~Element();

    // element's coordinates and size
    int x();
    int y();
    int width();
    int height();

    // pointer to element's instruction
    Instruction * instruction();

    // getters & setters
    Node * inputNode();
    void setInputNode(Node * node);
    Node * outputNode();
    void setOutputNode(Node * node);
    int line();
    void setLine(int line);

    // true if coordinates belong to element
    bool contains(int x, int y);
    
signals:
    
public slots:

protected:
    // coordinates
    int _x, _y;
    //size
    int _width, _height;
    // element's nodes
    Node * _inputNode, * _outputNode;
    // parent line index
    int _line;
    // instruction of element
    Instruction * _instruction;
    
};

#endif // ELEMENT_H
