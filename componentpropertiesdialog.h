#ifndef COMPONENTPROPERTIESDIALOG_H
#define COMPONENTPROPERTIESDIALOG_H

#include <QDialog>

#include "basicinstruction.h"
#include "comparatorinstruction.h"
#include "mathinstruction.h"
#include "timerinstruction.h"
#include "instruction.h"
#include "element.h"

namespace Ui {
class ComponentPropertiesDialog;
}

class ComponentPropertiesDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit ComponentPropertiesDialog(QWidget *parent = 0);
    virtual ~ComponentPropertiesDialog();

    // setting element for params editing
    void setElement(Element * element);
    // setting list of aliases of data tables' records
    void setDataAliases(QStringList aliases);

public slots:
    // dialog was accepted
    virtual void accept();
    // dialog was rejected
    virtual void reject();
    
private:
    Ui::ComponentPropertiesDialog *ui;

protected:
    // element for editing params
    Element * _element;
    // list of aliases
    QStringList _aliases;
};

#endif // COMPONENTPROPERTIESDIALOG_H
