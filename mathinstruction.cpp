#include "mathinstruction.h"

MathInstruction::MathInstruction(QString opcode, QString alias, QString dataSrc1, QString dataSrc2, QString output, QString imagePath, QObject *parent) :
    Instruction(opcode, alias, imagePath, parent)
{
    _type = MATH;
    _dataSrc1 = dataSrc1;
    _dataSrc2 = dataSrc2;
    _output = output;
}

QString MathInstruction::dataSrc1()
{
    return _dataSrc1;
}

void MathInstruction::setDataSrc1(QString dataSrc)
{
    _dataSrc1 = dataSrc;
}

QString MathInstruction::dataSrc2()
{
    return _dataSrc2;
}

void MathInstruction::setDataSrc2(QString dataSrc)
{
    _dataSrc2 = dataSrc;
}

QString MathInstruction::output()
{
    return _output;
}

void MathInstruction::setOutput(QString output)
{
    _output = output;
}

Instruction *MathInstruction::clone()
{
    // create copy of this object
    return new MathInstruction(opcode(), alias(), dataSrc1(), dataSrc2(), output(), imagePath(), parent());
}

void MathInstruction::save(QDataStream &stream)
{
    Instruction::save(stream);  // save predecessor's params
    // save current class params
    stream << _dataSrc1;
    stream << _dataSrc2;
    stream << _output;
}

void MathInstruction::load(QDataStream &stream)
{
    Instruction::load(stream);  // load predecessor's params
    // load current class params
    stream >> _dataSrc1;
    stream >> _dataSrc2;
    stream >> _output;
}
