#include "basicinstruction.h"

BasicInstruction::BasicInstruction(QString opcode, QString alias, QString dataSrc, QString imagePath, QObject *parent) :
    Instruction(opcode, alias, imagePath, parent)
{
    _type = BASIC;
    _dataSrc = dataSrc;
}

QString BasicInstruction::dataSrc()
{
    return _dataSrc;
}

void BasicInstruction::setDataSrc(QString dataSrc)
{
    _dataSrc = dataSrc;
}

Instruction *BasicInstruction::clone()
{
    return new BasicInstruction(opcode(), alias(), dataSrc(), imagePath(), parent());
}

void BasicInstruction::save(QDataStream &stream)
{
    Instruction::save(stream);  // store predecessor's params to stream
    stream << _dataSrc; // store data source to stream
}

void BasicInstruction::load(QDataStream &stream)
{
    Instruction::load(stream);  // load predecessor's params from stream
    stream >> _dataSrc; // load data souce from stream
}
