#ifndef MEMORYEDITOR_H
#define MEMORYEDITOR_H

#include <QDebug>
#include <QWidget>

#include "memoryrecord.h"
#include "memorytable.h"

namespace Ui {
class MemoryEditor;
}

class MemoryEditor : public QWidget
{
    Q_OBJECT
    
public:
    explicit MemoryEditor(QWidget *parent = 0);
    ~MemoryEditor();

    void setMemoryTable(MemoryTable * table);   // set data table for records editing

public slots:
    // dialog accepted
    void accept();
    // dialog rejected
    void reject();
    
private:
    Ui::MemoryEditor *ui;

protected:
    MemoryTable * _table;   // current data table

protected slots:
    void selectionChanged();    // user selected or deselected something in table
    void addBefore();       // add row before selection
    void addAfter();        // add row affter selection
    void addToEnd();        // add row to end of table
    void deleteRows();      // delete selected rows
    void updateTable();     // store entered data to data table
};

#endif // MEMORYEDITOR_H
