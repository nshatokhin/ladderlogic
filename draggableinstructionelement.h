#ifndef DRAGGABLEINSTRUCTIONELEMENT_H
#define DRAGGABLEINSTRUCTIONELEMENT_H

#include "draggableelement.h"

class DraggableInstructionElement : public DraggableElement
{
    Q_OBJECT
public:
    explicit DraggableInstructionElement(QWidget *parent = 0);
    ~DraggableInstructionElement();
    
signals:
    
public slots:
    void setInstruction(Instruction * instruction); // set element's instruction

protected:
    Instruction * _instruction; // instruction of element

    // mouse pressed
    void mousePressEvent(QMouseEvent *);
    // repainting
    void paintEvent(QPaintEvent *);
    
};

#endif // DRAGGABLEINSTRUCTIONELEMENT_H
