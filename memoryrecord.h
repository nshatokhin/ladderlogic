#ifndef MEMORYRECORD_H
#define MEMORYRECORD_H

#include <QObject>

class MemoryRecord
{
public:
    explicit MemoryRecord(QString inSlot, QString alias, QString value);    // construct record from given params
    MemoryRecord(QDataStream &stream);  // construct record and load params from stream
    MemoryRecord(const MemoryRecord& record);

    // getters of record's params
    QString inSlot() const;
    QString alias() const;
    QString value() const;

    // save and load functions
    void load(QDataStream &stream);
    void save(QDataStream &stream);
    
signals:
    
public slots:

protected:
    // record's params
    QString _inSlot;
    QString _alias;
    QString _value;
    
};

#endif // MEMORYRECORD_H
