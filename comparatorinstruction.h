#ifndef COMPARATORINSTRUCTION_H
#define COMPARATORINSTRUCTION_H

#include "instruction.h"

class ComparatorInstruction : public Instruction
{
    Q_OBJECT
public:
    explicit ComparatorInstruction(QString opcode, QString alias, QString dataSrc1, QString dataSrc2, QString output, QString imagePath, QObject *parent = 0);
    
    // getters & setters
    QString dataSrc1();
    void setDataSrc1(QString dataSrc);
    QString dataSrc2();
    void setDataSrc2(QString dataSrc);
    QString output();
    void setOutput(QString output);

    // returns adress of object's copy
    Instruction * clone();

    // save & load functions
    void save(QDataStream &stream);
    void load(QDataStream &stream);

signals:
    
public slots:

protected:
    // params
    QString _dataSrc1;
    QString _dataSrc2;
    QString _output;
};

#endif // COMPARATORINSTRUCTION_H
