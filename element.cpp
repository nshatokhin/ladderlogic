#include "element.h"

Element::Element(QObject *parent) :
    QObject(parent)
{
}

Element::Element(int x, int y, int width, int height, Node * left, Node * right, int line, Instruction *instruction, QObject *parent) :
    QObject(parent)
{
    _x = x;
    _y = y;
    _width = width;
    _height = height;
    _inputNode = left;
    _outputNode = right;
    _line = line;
    _instruction = instruction->clone();
    _instruction->setParent(this);
}

Element::~Element()
{
    delete _instruction;
}

int Element::x()
{
    return _x;
}

int Element::y()
{
    return _y;
}

int Element::width()
{
    return _width;
}

int Element::height()
{
    return _height;
}

Instruction * Element::instruction()
{
    return _instruction;
}

Node *Element::inputNode()
{
    return _inputNode;
}

void Element::setInputNode(Node *node)
{
    _inputNode = node;
}

Node *Element::outputNode()
{
    return _outputNode;
}

void Element::setOutputNode(Node *node)
{
    _outputNode = node;
}

int Element::line()
{
    return _line;
}

void Element::setLine(int line)
{
    _line = line;
}

bool Element::contains(int x, int y)
{
    // true, if point located between top left corner and bottom right
    return ((x >= _x) && (y >= _y) &&
            (x < _x + _width) && (y < _y + _height));
}
