#include "logiceditor.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    LogicEditor w;
    QApplication::connect(&w, SIGNAL(quit()), &a, SLOT(quit()));
    w.show();
    
    return a.exec();
}
