#ifndef TIMERINSTRUCTION_H
#define TIMERINSTRUCTION_H

#include "instruction.h"

class TimerInstruction : public Instruction
{
    Q_OBJECT
public:
    explicit TimerInstruction(QString opcode, QString alias, QString timeValue, QString imagePath, QObject *parent = 0);
    
    // getter & setter for instruction param
    QString timeValue();
    void setTimeValue(QString timeValue);

    // returns pointer to object's copy
    Instruction * clone();

    // save & load functions
    void save(QDataStream &stream);
    void load(QDataStream &stream);

signals:
    
public slots:

protected:
    QString _timeValue; // instruction param
    
};

#endif // TIMERINSTRUCTION_H
