#ifndef MEMORYTABLE_H
#define MEMORYTABLE_H

#include <QDebug>
#include <QObject>
#include <QStringList>

#include "memoryrecord.h"

class MemoryTable : public QObject
{
    Q_OBJECT
public:
    explicit MemoryTable(QObject *parent = 0);
    ~MemoryTable();
    
    QList<MemoryRecord> records();  // return list of table's records
    void addRecord(MemoryRecord record);    // add record to table
    void clear();   // clear table

    // return list of aliases that exist in table
    QStringList aliases();

    // save & load functions
    void load(QDataStream &stream);
    void save(QDataStream &stream);

    // exports table to XML
    QString exportToXml(QString tagName);

signals:
    
public slots:

protected:
    QList<MemoryRecord> _records;   // list of table's records
    
};

#endif // MEMORYTABLE_H
