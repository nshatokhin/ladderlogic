#include "memoryrecord.h"

MemoryRecord::MemoryRecord(QString inSlot, QString alias, QString value)
{
    _inSlot = inSlot;
    _alias = alias;
    _value = value;
}

MemoryRecord::MemoryRecord(QDataStream &stream)
{
    load(stream);   // load record from stream
}

MemoryRecord::MemoryRecord(const MemoryRecord &record)
{
    // making copy of record
    _inSlot = record.inSlot();
    _alias = record.alias();
    _value = record.value();
}

QString MemoryRecord::inSlot() const
{
    return _inSlot;
}

QString MemoryRecord::alias() const
{
    return _alias;
}

QString MemoryRecord::value() const
{
    return _value;
}

void MemoryRecord::load(QDataStream &stream)
{
    // load record params from stream
    stream >> _inSlot;
    stream >> _alias;
    stream >> _value;
}

void MemoryRecord::save(QDataStream &stream)
{
    // save record params to stream
    stream << _inSlot;
    stream << _alias;
    stream << _value;
}
