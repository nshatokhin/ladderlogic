#include "logiceditor.h"
#include "ui_logiceditor.h"

LogicEditor::LogicEditor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LogicEditor)
{
    ui->setupUi(this);

    // set instructions to draggable elements from top panel
    ui->xicInstruction->setInstruction(instructionByOpcode("XIC"));
    ui->xioInstruction->setInstruction(instructionByOpcode("XIO"));
    ui->oteInstruction->setInstruction(instructionByOpcode("OTE"));
    ui->otlInstruction->setInstruction(instructionByOpcode("OTL"));
    ui->otuInstruction->setInstruction(instructionByOpcode("OTU"));
    ui->osrInstruction->setInstruction(instructionByOpcode("OSR"));

    ui->tonInstruction->setInstruction(instructionByOpcode("TON"));
    ui->tofInstruction->setInstruction(instructionByOpcode("TOF"));
    ui->ctuInstruction->setInstruction(instructionByOpcode("CTU"));
    ui->ctdInstruction->setInstruction(instructionByOpcode("CTD"));
    ui->resInstruction->setInstruction(instructionByOpcode("RES"));

    ui->equInstruction->setInstruction(instructionByOpcode("EQU"));
    ui->neqInstruction->setInstruction(instructionByOpcode("NEQ"));
    ui->lesInstruction->setInstruction(instructionByOpcode("LES"));
    ui->leqInstruction->setInstruction(instructionByOpcode("LEQ"));
    ui->grtInstruction->setInstruction(instructionByOpcode("GRT"));
    ui->geqInstruction->setInstruction(instructionByOpcode("GEQ"));
    ui->meqInstruction->setInstruction(instructionByOpcode("MEQ"));
    ui->limInstruction->setInstruction(instructionByOpcode("LIM"));

    ui->addInstruction->setInstruction(instructionByOpcode("ADD"));
    ui->subInstruction->setInstruction(instructionByOpcode("SUB"));
    ui->mulInstruction->setInstruction(instructionByOpcode("MUL"));
    ui->divInstruction->setInstruction(instructionByOpcode("DIV"));

    ui->lineTool->setIcon(QPixmap(":/assets/tools/line.png"));  // set icon for line draggable element

    // creating dialog for editing element properties
    componentPropertiesDialog = new ComponentPropertiesDialog(this);
    connect(ui->drawArea, SIGNAL(setProperties(Element*)), this, SLOT(changeComponentProperties(Element*)));

    // creating dialog for edit data tables
    memoryEditor = new MemoryEditor(this);
    //creating data tables
    inputsTable = new MemoryTable(this);
    outputsTable = new MemoryTable(this);
    ramTable = new MemoryTable(this);
    romTable = new MemoryTable(this);

    // connect signals of menubar items to slots
    connect(ui->actionInput_Map, SIGNAL(triggered()), this, SLOT(showInputsTable()));
    connect(ui->actionOutput_Map, SIGNAL(triggered()), this, SLOT(showOutputsTable()));
    connect(ui->actionRAM_Map, SIGNAL(triggered()), this, SLOT(showRamTable()));
    connect(ui->actionROM_Map, SIGNAL(triggered()), this, SLOT(showRomTable()));

    connect(ui->actionNew, SIGNAL(triggered()), this, SLOT(clear()));
    connect(ui->actionLoad, SIGNAL(triggered()), this, SLOT(load()));
    connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(save()));
    connect(ui->actionExport, SIGNAL(triggered()), this, SLOT(exportXml()));
    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(exit()));
}

LogicEditor::~LogicEditor()
{
    delete ui;

    delete componentPropertiesDialog;

    delete memoryEditor;
    delete inputsTable;
    delete outputsTable;
    delete ramTable;
    delete romTable;
}

void LogicEditor::changeComponentProperties(Element *element)
{
    // collect aliases from all data tables
    QStringList dataAliases;
    dataAliases << QString();
    dataAliases << inputsTable->aliases();
    dataAliases << outputsTable->aliases();
    dataAliases << ramTable->aliases();
    dataAliases << romTable->aliases();

    componentPropertiesDialog->setDataAliases(dataAliases); // set collected aliases to dialog
    componentPropertiesDialog->setElement(element); // set element that triggered signal to dialog

    componentPropertiesDialog->exec();  //show dialog
}

void LogicEditor::showInputsTable()
{
    memoryEditor->setMemoryTable(inputsTable);  // set inputs table to dialog
    memoryEditor->show();   // show dialog
}

void LogicEditor::showOutputsTable()
{
    memoryEditor->setMemoryTable(outputsTable);  // set outputs table to dialog
    memoryEditor->show();   // show dialog
}

void LogicEditor::showRamTable()
{
    memoryEditor->setMemoryTable(ramTable);  // set ram table to dialog
    memoryEditor->show();   // show dialog
}

void LogicEditor::showRomTable()
{
    memoryEditor->setMemoryTable(romTable);  // set rom table to dialog
    memoryEditor->show();   // show dialog
}

void LogicEditor::clear()   // reset app to initial state
{
    // clearing draw area
    ui->drawArea->clear();
    // clearing data tables
    inputsTable->clear();
    outputsTable->clear();
    ramTable->clear();
    romTable->clear();
}

void LogicEditor::save()    // saving
{
    // show dialog for selection file for saving
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Save diagram"), QDir::homePath(), tr("LDR files (*.ldr);;All files (*.*)"));
    // if file not selected, return
    if(fileName.isEmpty()) return;
    // file selected. Opening it
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly))
    {
        qDebug() << "Can't open file:" << fileName;
        return;
    }
    // creating data stream
    QDataStream out(&file);
    out.setVersion(QDataStream::Qt_5_0);    // set data stream's format

    // saving data tables
    inputsTable->save(out);
    outputsTable->save(out);
    ramTable->save(out);
    romTable->save(out);
    // saving draw area
    ui->drawArea->save(out);
    // save and close file
    file.close();
}

void LogicEditor::load()    // loading
{
    // select file for loading
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Load diagram"), QDir::homePath(), tr("LDR files (*.ldr);;All files (*.*)"));
    // if file is not selected, exit
    if(fileName.isEmpty()) return;
    // if file is selected, try to open it
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly))
    {
        qDebug() << "Can't open file:" << fileName;
        return;
    }
    // creating stream
    QDataStream in(&file);
    in.setVersion(QDataStream::Qt_5_0); // setting stream's data format
    // load data tables
    inputsTable->load(in);
    outputsTable->load(in);
    ramTable->load(in);
    romTable->load(in);
    // load draw area
    ui->drawArea->load(in);
    // close file
    file.close();
}

void LogicEditor::exportXml() // exporting to xml
{
    // select file for saving XML
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Export XML"), QDir::homePath(), tr("XML files (*.xml);;All files (*.*)"));
    // exit if file is not selected
    if(fileName.isEmpty()) return;
    // if file selected, try to open it
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "Can't open file:" << fileName;
        return;
    }
    // creating data stream
    QTextStream out(&file);
    out << ui->drawArea->toXml();   // saving xml of draw area to stream
    // save xml of data tables to stream
    out << inputsTable->exportToXml("inputmap");
    out << outputsTable->exportToXml("outputmap");
    out << ramTable->exportToXml("rammap");
    out << romTable->exportToXml("rommap");
    // save and close file
    file.close();
}

void LogicEditor::exit()
{
    emit quit();
}


void LogicEditor::closeEvent(QCloseEvent *) // if main window closed
{
    exit(); // exit application
}
