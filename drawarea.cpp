#include "drawarea.h"

DrawArea::DrawArea(QWidget *parent) :
    QWidget(parent)
{
    setAcceptDrops(true);

    scrollStep = DEFAULT_SCROLL_STEP;

    rungs.clear();

    // creating scrollbar
    _scroll = new QScrollBar(Qt::Vertical, this);
    _scroll->setMaximum(0);
    _scroll->setEnabled(false);

    // if user moved scrollbar
    connect(_scroll, SIGNAL(valueChanged(int)), this, SLOT(scrolling(int)));

    frameY = 0;
    areaHeight = 0;

    _mousePressed = false;
    _activeRung = NULL;
}

DrawArea::~DrawArea()
{
    removeRungs();

    delete _scroll;
}

QString DrawArea::toXml()
{
    QString xml;

    for(int i=0;i<rungs.size();i++)
    {
        // making xml for each rung
        xml.append("<rung>\n");
        xml.append(QString("<num>%1</num>\n").arg(i+1));
        xml.append(rungs[i]->toXml());
        xml.append("</rung>\n\n");
    }

    return xml;
}

void DrawArea::clear() // clear area
{
    removeRungs();

    repaint();
}

void DrawArea::load(QDataStream &stream)
{
    clear();    // clean area

    int rungsCount;
    // loading rungs count
    stream >> rungsCount;
    // for each rung
    for(int i=0;i<rungsCount;i++)
    {
        // create rung
        Rung * rung = new Rung(stream);
        connect(rung, SIGNAL(resized()), this, SLOT(moveRungs()));
        // insert rung to rungs list
        rungs.insert(i, rung);
        // increase area's height
        areaHeight += rung->height();
    }

    updateScroll(); // updating scrollbar
    repaint();  // repaint widget
}

void DrawArea::save(QDataStream &stream)
{
    stream << rungs.size(); // save rungs count

    for(int i=0;i<rungs.size();i++)
    {
        rungs[i]->save(stream); // save each rung
    }
}

void DrawArea::dragEnterEvent(QDragEnterEvent * event) // if user drags item to area
{
    event->acceptProposedAction();  // allow to drop item
}

void DrawArea::dropEvent(QDropEvent * event) // user dropped item
{
    int x = event->pos().x();
    int y = event->pos().y();
    int frameLeft = 0;
    int frameTop = frameY;
    int frameWidth = width() - _scroll->width();
    int frameHeight = height();

    // searching rungs which will catch dropped item
    for(int i=0;i<rungs.size();i++)
    {
        // if rung contains drop position
        if(rungs[i]->contains(event->pos().x(), event->pos().y() + frameY))
        {
            qDebug() << "addElement to rung: " << i;

            // get mimeData from drop event
            ElementMimeData * elementData = (ElementMimeData *) event->mimeData();

            if(elementData->type() == ElementMimeData::INSTRUCTION) // if it is instruction
            {
                InstructionMimeData * mimeData = (InstructionMimeData *) elementData;   // convert given mimeData to instruction's mime data
                rungs[i]->addElement(x, y, frameLeft, frameTop, frameWidth, frameHeight, mimeData->instruction()); // add element to rung
            }
            else if(elementData->type() == ElementMimeData::TOOL)   // is it is tool
            {
                ToolMimeData * toolData = (ToolMimeData *) elementData; // convert given mimeData to tool's mimeData

                switch(toolData->tool())    // check tool's type
                {
                    case ToolMimeData::LINE:    // if it is a line
                        rungs[i]->addLine(x, y, frameLeft, frameTop, frameWidth, frameHeight);  // add line to rung

                        break;
                }
            }

            break;
        }
    }

    event->acceptProposedAction();  // accept dropping

    repaint();  // repainting widget
}

void DrawArea::paintEvent(QPaintEvent *)
{
    QPainter painter(this); // making painter

    painter.fillRect(0, 0, width() - _scroll->width(), height(), Qt::gray); // drawing area's gray background

    for(int i=0;i<rungs.size();i++) // for each rung
    {
        rungs[i]->draw(painter, 0, frameY, width() - _scroll->width(), height());   // draw this rung
        rungs[i]->drawNodes(painter, 0, frameY, width() - _scroll->width(), height());  // draw it's nodes
    }
}

void DrawArea::mousePressEvent(QMouseEvent * event)
{
    _mousePressed = true;   // mouse is pressed now
    _lastMousePosition = event->pos(); // save position of mouse pointer

    /*for(int i=0;i<rungs.size();i++)
    {
        if(rungs[i]->contains(event->x(), event->y() + frameY))
        {
            _activeRung = rungs[i];
            _activeRung->mousePressed(event->x(), event->y() + frameY);

            break;
        }
    }*/
}

void DrawArea::mouseReleaseEvent(QMouseEvent * )
{
    _mousePressed = false;  // mouse is not pressed anymore

    /*if(_activeRung != NULL && _activeRung->contains(event->x(), event->y() + frameY))
        _activeRung->mouseReleased(event->x(), event->y() + frameY);

    _activeRung = NULL;*/
}

void DrawArea::mouseMoveEvent(QMouseEvent * event)
{
    if(_mousePressed)
    {
        //_activeRung->mouseMoving(event->x(), event->y() + frameY);

        _lastMousePosition = event->pos();  // save mouse pointer's position

        repaint();
    }
}

void DrawArea::mouseDoubleClickEvent(QMouseEvent * event) // mouse double click
{
    for(int i=0;i<rungs.size();i++) // for each rung
    {
        if(rungs[i]->contains(event->x(), event->y() + frameY)) // if click's position inside of rung
        {
            QList<Element *> elements = rungs[i]->elements();

            for(int j=0;j<elements.size();j++)  // for each element of this rung
            {
                if(elements[j]->contains(event->x() - rungs[i]->x(), event->y() - rungs[i]->y() + frameY)) // if click's position inside of element
                {
                    emit setProperties(elements[j]);    // change properties of this element

                    return;
                }
            }
            // else add rung after current
            addRung(i+1);

            return;
        }
    }
    //if no rungs or neither rung contains clicks position
    addRung(rungs.size());  // add rung to end
}

void DrawArea::resizeEvent(QResizeEvent *)
{
    _scroll->resize(16, height());  // setting scrollbar's width and height
    _scroll->move(width()-_scroll->width(), 0); // moving scrollbar to right of draw area
}

void DrawArea::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);   // create menu

    menu.addAction(tr("Add rung to end"), this, SLOT(addRungToEnd()));  // add menu item

    for (int i = 0; i < rungs.size(); i++) // for each rung
    {
        if(rungs[i]->contains(event->x(), event->y()))  // if rung contains mouse click position
        {
            _selectedRung = i;  // save selected rung
            menu.addAction(tr("Add rung before current"), this, SLOT(addRungBeforeSelected()));   // add menu item
            menu.addAction(tr("Add rung after current"), this, SLOT(addRungAfterSelected()));     // add menu item
            menu.addAction(tr("Delete rung"), this, SLOT(deleteRung()));      // add menu item
            break;  // leave cycle
        }
    }

    menu.exec(event->globalPos());  // show context menu
}

void DrawArea::drawNodes(QPainter &)
{
}

void DrawArea::updateScroll()   // updating scrollbar's step count
{
    if(areaHeight > height())   // if draw area's real height bigger than widget's height
    {
        _scroll->setEnabled(true);  // enabling scrollbar

        // calculating number of steps
        int visibleSteps = (int) (height() / scrollStep);
        int areaSteps = (int) ceil((double) areaHeight / (double) scrollStep);
        int invisibleSteps = areaSteps - visibleSteps;
        // set number of space to scrollbar
        _scroll->setMaximum(invisibleSteps);
    }
    else    // else
    {
        _scroll->setMaximum(0);     // set 0 steps
        _scroll->setEnabled(false); // disable scrollbar
    }
}

void DrawArea::removeRungs()    // removing rungs
{
    for(int i=0;i<rungs.length();i++)   // for each rung
        delete rungs[i];    // destroy rung
    rungs.clear();  // and clear rungs list
    areaHeight = 0; // area's real height is 0 now

    updateScroll(); // updating scrollbar
}

void DrawArea::moveRungs()
{
    int previousRungsHeight = 0; // sum of heights of all rungs before current

    areaHeight = 0;
    for(int i=0;i<rungs.size();i++) // for each rung
    {
        rungs[i]->move(0, previousRungsHeight); // moving rung to the end of previous
        previousRungsHeight += rungs[i]->height();  // increasing heights of rungs by current for know end of previous rungs
        areaHeight += rungs[i]->height();   // increasing area's real height
    }

    updateScroll(); // updating scrollbar
}

void DrawArea::scrolling(int value)
{
    frameY = value * scrollStep;    // calculate offset

    repaint();  // repaint widget
}

void DrawArea::recalculateNodesIndexes()
{
    int maxNodeCount = 0;   // nodes count in previous rungs

    for(int i=0;i<rungs.size();i++) // for each rung
    {
        rungs[i]->setStartNodeNumber(maxNodeCount); // set start node to maximum of previous nodes

        maxNodeCount += rungs[i]->maximumNodeNumber();  // increase maxNodeCount by this rung's nodes count
    }
}

void DrawArea::addRungBeforeSelected()
{
    addRung(_selectedRung);
}

void DrawArea::addRungAfterSelected()
{
    addRung(_selectedRung + 1);
}

void DrawArea::addRungToEnd()
{
    addRung(rungs.size());
}

void DrawArea::deleteRung()
{
    if(_selectedRung < 0 || _selectedRung >= rungs.size()) return;  // if selected rung's id is out of range, return

    Rung * rung = rungs.takeAt(_selectedRung);  // take rung from list

    // disconnect slots from it's signals
    disconnect(rung, SIGNAL(resized()), this, SLOT(moveRungs()));
    disconnect(rung, SIGNAL(nodeAdded()), this, SLOT(recalculateNodesIndexes()));
    // free memory
    delete rung;

    updateScroll(); // updating scrollbar
    moveRungs();    // reposition another rungs
    recalculateNodesIndexes();  // update node numbers in rungs

    repaint();  // repaint widget
}


void DrawArea::addRung(int rungId)
{
    int y = 0;
    // calculating new rung's position
    for(int i=0;i<rungs.size() && i<rungId;i++) // for each rungs before rungId
    {
        y += rungs[i]->height(); // increase y by this rung's height
    }

    // creating new rung in found position
    Rung * rung = new Rung(0, y, width() - _scroll->width());
    // connecting it's signals to slots
    connect(rung, SIGNAL(resized()), this, SLOT(moveRungs()));
    connect(rung, SIGNAL(nodeAdded()), this, SLOT(recalculateNodesIndexes()));
    // inserting new rung to list
    rungs.insert(rungId, rung);

    updateScroll(); // updating scroll
    moveRungs();    // repositioning existing rungs

    repaint();  // repaint widget
}
