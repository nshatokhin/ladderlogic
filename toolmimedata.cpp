#include "toolmimedata.h"

ToolMimeData::ToolMimeData() : ElementMimeData()
{
    _type = TOOL;   // it's tool
    _tool = LINE;   // it's line tool
}

ToolMimeData::Tools ToolMimeData::tool()
{
    return _tool;
}

void ToolMimeData::setTool(ToolMimeData::Tools tool)
{
    _tool = tool;
}
