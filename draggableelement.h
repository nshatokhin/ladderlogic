#ifndef DRAGGABLEELEMENT_H
#define DRAGGABLEELEMENT_H

#include <QDebug>
#include <QDrag>
#include <QMimeData>
#include <QMouseEvent>
#include <QPainter>
#include <QWidget>

#include "instruction.h"
#include "instructionmimedata.h"
#include "basicinstruction.h"

class DraggableElement : public QWidget
{
    Q_OBJECT
public:
    explicit DraggableElement(QWidget *parent = 0);
    ~DraggableElement();

    // possible types of elements
    enum Type {INSTRUCTION, TOOL};
    
signals:
    
public slots:

protected:
    // type of element
    Type _type;
    
};

#endif // DRAGGABLEELEMENT_H
