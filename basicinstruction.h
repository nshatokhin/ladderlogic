#ifndef BASICINSTRUCTION_H
#define BASICINSTRUCTION_H

#include <QPixmap>

#include "instruction.h"

class BasicInstruction : public Instruction
{
    Q_OBJECT
public:
    BasicInstruction(QString opcode, QString alias, QString dataSrc, QString imagePath, QObject *parent = 0);

    // getter & setter for dataSrc
    QString dataSrc();
    void setDataSrc(QString dataSrc);

    // returns adress of object's copy
    Instruction * clone();

    // save & load functions
    void save(QDataStream &stream);
    void load(QDataStream &stream);

protected:
    // Data Source param
    QString _dataSrc;
};

#endif // BASICINSTRUCTION_H
