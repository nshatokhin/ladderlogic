#include "node.h"

Node::Node(QObject *parent) :
    QObject(parent)
{
}

Node::Node(int x, int y, QObject *parent) :
    QObject(parent)
{
    _x = x;
    _y = y;
}

int Node::x()
{
    return _x;
}

int Node::y()
{
    return _y;
}

void Node::move(int x, int y)   // move node to new position
{
    _x = x;
    _y = y;
}

void Node::addLeftElement(Element * element)    // connect element that located at left of node
{
    if(!left_elements.contains(element))    // if left elements list don't have current
        left_elements.push_back(element);   // add it to list
}

void Node::addRightElement(Element *element)    // connect element that located at right of node
{
    if(!right_elements.contains(element))   // if right elements list don't have current
        right_elements.push_back(element);  // add it to list
}

void Node::removeLeftElement(Element *element)  // disconnect element that located at left of node
{
    if(left_elements.contains(element)) // if left elements list have current
        left_elements.removeAll(element);   // remove it from list
}

void Node::removeRightElement(Element *element) // disconnect element that located at right of node
{
    if(right_elements.contains(element))    // if right elements list have current
        right_elements.removeAll(element);  // remove it from list
}

QList<Element *> Node::leftElements()   // return node's left elements
{
    return left_elements;
}

void Node::setLeftElements(QList<Element *> elements)   // set node's left elements
{
    left_elements = elements;
}

QList<Element *> Node::rightElements()  // return node's right elements
{
    return right_elements;
}

void Node::setRightElements(QList<Element *> elements)  // set node's right elements
{
    right_elements = elements;
}

int Node::index()   // return node's index
{
    return _index;
}

void Node::setIndex(int index)  // set node's index
{
    _index = index;
}

void Node::clearLeftElements()  // disconnect all left elements
{
    left_elements.clear();
}

void Node::clearRightElements() // disconnect all right elements
{
    right_elements.clear();
}
