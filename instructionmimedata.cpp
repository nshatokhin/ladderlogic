#include "instructionmimedata.h"

InstructionMimeData::InstructionMimeData() : ElementMimeData()
{
    _type = INSTRUCTION;
}

Instruction * InstructionMimeData::instruction()
{
    return _instruction;
}

void InstructionMimeData::setInstruction(Instruction * instruction)
{
    _instruction = instruction;
}
