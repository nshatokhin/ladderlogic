#include "timerinstruction.h"

TimerInstruction::TimerInstruction(QString opcode, QString alias, QString timeValue, QString imagePath, QObject *parent) :
    Instruction(opcode, alias, imagePath, parent)
{
    _type = TIMER;
    _timeValue = timeValue;
}

QString TimerInstruction::timeValue()
{
    return _timeValue;
}

void TimerInstruction::setTimeValue(QString timeValue)
{
    _timeValue = timeValue;
}

Instruction * TimerInstruction::clone() // make copy of this object
{
    return new TimerInstruction(opcode(), alias(), timeValue(), imagePath(), parent());
}


void TimerInstruction::save(QDataStream &stream)
{
    Instruction::save(stream);  // save predecessor's params
    stream << _timeValue;   // save time value
}

void TimerInstruction::load(QDataStream &stream)
{
    Instruction::load(stream);  // load predecessor's params
    stream >> _timeValue;   // load time value
}
