#ifndef LINE_H
#define LINE_H

#include <QObject>

#include "element.h"
#include "node.h"

class Line : public QObject
{
    Q_OBJECT
public:
    explicit Line(int x, int y, int width, int height, Node * inputNode, Node * outputNode, Line * parentLineLeft, Line * parentLineRight, int index = 0, QObject *parent = 0);
    ~Line();

    // getters & setters
    int x();
    int y();
    int width();
    int height();
    int index();
    void setIndex(int index);

    Node * inputNode();
    void setInputNode(Node * node);
    Node * outputNode();
    void setOutputNode(Node * node);
    Line * parentLineLeft();
    void setParentLineLeft(Line * line);
    Line * parentLineRight();
    void setParentLineRight(Line * line);

    // true if x and y belongs to line
    bool contains(int x, int y);
    // true if line intersects with current
    bool intersection(Line * line);

    // move and resize funtions
    void move(int x, int y);
    void resize(int width, int height);

    // returns list of elements that belong to line
    QList<Element *> elements();
    // add element to line
    void addElement(Element * element);

    // same for nodes
    QList<Node *> nodes();
    void addNode(Node * node);
    void clearNodes();

    // return pointers to nearest left and right nodes or NULL if not exists
    Node * findLeftNode(int x, int y);
    Node * findRightNode(int x, int y);

    // returns pointer to nearest right element or NULL if not exists
    Element * findRightElement(int x, int y);

    // returns list of elements that belongs to line and located between two points
    QList<Element *> findElementsBetweenTwoPoints(int x1, int y1, int x2, int y2);

signals:
    
public slots:

protected:
    int _x, _y;
    int _width, _height;
    int _index;
    // input & output nodes
    Node * _inputNode, * _outputNode;

    // list of pointers to elements that belongs to line
    QList<Element *> _elements;
    // list of pointers to nodes that belongs to line
    QList<Node *> _nodes;
    // parent lines where current started and finished
    Line * _parentLineLeft, * _parentLineRight;

    bool _movingLineLeftBorder;
};

#endif // LINE_H
