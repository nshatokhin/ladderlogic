#include "memorytable.h"

MemoryTable::MemoryTable(QObject *parent) :
    QObject(parent)
{
}

MemoryTable::~MemoryTable()
{
    clear();
}

QList<MemoryRecord> MemoryTable::records()
{
    return _records;
}

void MemoryTable::addRecord(MemoryRecord record)
{
    _records.push_back(record); // add record to end of data table
}

void MemoryTable::clear()
{
    _records.clear();   // clear records list
}

QStringList MemoryTable::aliases()  // return alias list of current data table
{
    QStringList aliases;
    // collect aliases from records
    for(int i=0;i<_records.length();i++)
        aliases.push_back(_records[i].alias());
    // and return it
    return aliases;
}

void MemoryTable::load(QDataStream &stream) // load data table from stream
{
    clear();    // clear data table

    int recordsCount;
    stream >> recordsCount; // load records count
    // load each record
    for(int i=0;i<recordsCount;i++)
        addRecord(MemoryRecord(stream));
}

void MemoryTable::save(QDataStream &stream) // save data table to stream
{
    stream << _records.length();    // save records count
    // save each record
    for(int i=0;i<_records.length();i++)
        _records[i].save(stream);
}

QString MemoryTable::exportToXml(QString tagName)   // export table to XML
{
    QString xml;

    xml.append(QString("<%1>\n").arg(tagName)); // opening tag

    for(int i=0;i<_records.length();i++)
    {
        // tags of each row
        xml.append("<row>\n");
        xml.append(QString("<alias>%1</alias>\n").arg(_records[i].alias()));
        xml.append(QString("<input_slot>%1</input_slot>\n").arg(_records[i].inSlot()));
        xml.append(QString("<input_val>%1</input_val>\n").arg(_records[i].value()));
        xml.append("</row>\n");
    }

    xml.append(QString("</%1>\n\n").arg(tagName));  // closing tag

    return xml;
}
