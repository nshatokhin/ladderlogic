#include "comparatorinstruction.h"

ComparatorInstruction::ComparatorInstruction(QString opcode, QString alias, QString dataSrc1, QString dataSrc2, QString output, QString imagePath, QObject *parent) :
    Instruction(opcode, alias, imagePath, parent)
{
    _type = COMPARATOR;
    _dataSrc1 = dataSrc1;
    _dataSrc2 = dataSrc2;
    _output = output;
}

QString ComparatorInstruction::dataSrc1()
{
    return _dataSrc1;
}

void ComparatorInstruction::setDataSrc1(QString dataSrc)
{
    _dataSrc1 = dataSrc;
}

QString ComparatorInstruction::dataSrc2()
{
    return _dataSrc2;
}

void ComparatorInstruction::setDataSrc2(QString dataSrc)
{
    _dataSrc2 = dataSrc;
}

QString ComparatorInstruction::output()
{
    return _output;
}

void ComparatorInstruction::setOutput(QString output)
{
    _output = output;
}

Instruction *ComparatorInstruction::clone()
{
    return new ComparatorInstruction(opcode(), alias(), dataSrc1(), dataSrc2(), output(), imagePath(), parent());
}

void ComparatorInstruction::save(QDataStream &stream)
{
    // save predecessor's params to stream
    Instruction::save(stream);
    // save current's params to stream
    stream << _dataSrc1;
    stream << _dataSrc2;
    stream << _output;
}

void ComparatorInstruction::load(QDataStream &stream)
{
    // load predecessor's params from stream
    Instruction::load(stream);
    // load current's params's from stream
    stream >> _dataSrc1;
    stream >> _dataSrc2;
    stream >> _output;
}
