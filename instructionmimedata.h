#ifndef INSTRUCTIONMIMEDATA_H
#define INSTRUCTIONMIMEDATA_H

#include "elementmimedata.h"
#include "instruction.h"

// mimedata needed for drag & drop. We will get data from it, when user drops item. We will detect item by it and get params from it

class InstructionMimeData : public ElementMimeData
{
    Q_OBJECT
public:
    explicit InstructionMimeData();

    // getter & setter
    Instruction * instruction();
    void setInstruction(Instruction * instruction);
    
signals:
    
public slots:

protected:
    Instruction * _instruction;
    
};

#endif // INSTRUCTIONMIMEDATA_H
