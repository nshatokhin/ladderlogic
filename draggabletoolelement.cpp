#include "draggabletoolelement.h"

DraggableToolElement::DraggableToolElement(QWidget *parent) :
    DraggableElement(parent)
{
}

QPixmap DraggableToolElement::icon()
{
    return _icon;
}

void DraggableToolElement::setIcon(QPixmap icon)
{
    _icon = icon;
}

void DraggableToolElement::mousePressEvent(QMouseEvent * event)
{
    if (event->button() == Qt::LeftButton
            && event->pos().x() > 0 && event->pos().y() > 0 && event->pos().x() < width() && event->pos().y() < height())
    {   // if it is left button of mouse and pointer's position is above current element
            // prepare drag
            QDrag *drag = new QDrag(this);

            ToolMimeData *mimeData = new ToolMimeData();
            // set mime data and icon near mouse pointer
            drag->setMimeData(mimeData);
            drag->setPixmap(_icon);
            // start drag
            drag->exec();
    }
}

void DraggableToolElement::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    // draw tool icon
    painter.drawPixmap(width()/2 - _icon.width()/2, height()/2 - _icon.height()/2, _icon.width(), _icon.height(), _icon);
}
