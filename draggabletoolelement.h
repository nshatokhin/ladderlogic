#ifndef DRAGGABLETOOLELEMENT_H
#define DRAGGABLETOOLELEMENT_H

#include "draggableelement.h"
#include "toolmimedata.h"

class DraggableToolElement : public DraggableElement
{
    Q_OBJECT
public:
    explicit DraggableToolElement(QWidget *parent = 0);
    
    // getter & setter
    QPixmap icon();
    void setIcon(QPixmap icon);

signals:
    
public slots:

protected:
    QPixmap _icon;  // element's icon

    // mouse pressed
    void mousePressEvent(QMouseEvent *);
    // repainting
    void paintEvent(QPaintEvent *);
    
};

#endif // DRAGGABLETOOLELEMENT_H
