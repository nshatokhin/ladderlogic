#ifndef DRAWAREA_H
#define DRAWAREA_H

#include <cmath>

#include <QDebug>
#include <QDragEnterEvent>
#include <QMenu>
#include <QMimeData>
#include <QPainter>
#include <QScrollBar>
#include <QWidget>

#include "elementmimedata.h"
#include "instructionmimedata.h"
#include "toolmimedata.h"
#include "rung.h"

#define DEFAULT_SCROLL_STEP 50

class DrawArea : public QWidget
{
    Q_OBJECT
public:
    explicit DrawArea(QWidget *parent = 0);
    ~DrawArea();
    
    QString toXml();    // export diagrams to XML

    void clear(); // clear DrawArea to initial state
    // save & load functions
    void load(QDataStream &stream);
    void save(QDataStream &stream);

signals:
    void setProperties(Element *element);   // edit element's properties
    
public slots:
    void addRung(int rungId);   // add rung before rungId

protected:
    int scrollStep;     // how much pixels of DrawArea in one scrollbar step
    int frameY;     // current offset
    int areaHeight; // area's height

    QList<Rung *> rungs;    // list of rungs

    QScrollBar * _scroll;   // scrollbar

    bool _mousePressed;     // mouse button pressed now
    Rung * _activeRung;     // mouse was pressed above this rung
    QPoint _lastMousePosition;      // last position of mouse pointer

    int _selectedRung;  // rung selected by opening context menu on it

    void dragEnterEvent(QDragEnterEvent *); // user moved item to widget
    void dropEvent(QDropEvent *);   // user dropped item above widget
    void paintEvent(QPaintEvent *); // repainting
    void mousePressEvent(QMouseEvent *);    // mouse pressed
    void mouseReleaseEvent(QMouseEvent *);  // mouse released
    void mouseMoveEvent(QMouseEvent *);     // mouse moving
    void mouseDoubleClickEvent(QMouseEvent *);  //mouse double clicked
    void resizeEvent(QResizeEvent *);       // widget resized
    void contextMenuEvent(QContextMenuEvent *event);    // right mouse button clicked
    
    void drawNodes(QPainter &painter);  // draw node marks

    void updateScroll();    // calculate and set scroll size
    void removeRungs();     // destroy rungs

protected slots:
    void moveRungs();   // move rungs after one resized or added
    void scrolling(int value);  // do scrolling
    void recalculateNodesIndexes();     // set start node index for each rung

    void addRungBeforeSelected();   // add rung before selected
    void addRungAfterSelected();    // add rung after selected
    void addRungToEnd();            // add rung to end
    void deleteRung();              // delete selected rung
};

#endif // DRAWAREA_H
