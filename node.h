#ifndef NODE_H
#define NODE_H

#include <QObject>

#include "element.h"

class Element;

class Node : public QObject
{
    Q_OBJECT
public:
    explicit Node(QObject *parent = 0);
    Node(int x, int y, QObject *parent = 0);

    // node's coordinates getters
    int x();
    int y();

    // move node
    void move(int x, int y);

    // connect element to node
    void addLeftElement(Element *element);
    void addRightElement(Element *element);
    // remove connected element
    void removeLeftElement(Element *element);
    void removeRightElement(Element *element);

    // get & set list of connected elements
    QList<Element *> leftElements();
    void setLeftElements(QList<Element *> elements);
    QList<Element *> rightElements();
    void setRightElements(QList<Element *> elements);
    // getter and setter for node's index
    int index();
    void setIndex(int index);

    // clear connected elements
    void clearLeftElements();
    void clearRightElements();
    
signals:
    
public slots:

protected:
    int _x, _y; // node coordinates
    int _index; // node index
    QList<Element *> left_elements, right_elements; // connected elements

};

#endif // NODE_H
