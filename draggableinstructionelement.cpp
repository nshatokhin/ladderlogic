#include "draggableinstructionelement.h"

DraggableInstructionElement::DraggableInstructionElement(QWidget *parent) :
    DraggableElement(parent)
{
    _type = INSTRUCTION;
    _instruction = NULL;
}

DraggableInstructionElement::~DraggableInstructionElement()
{
    if(_instruction != NULL)    // destroy instruction if set
    {
        delete _instruction;
        _instruction = NULL;
    }
}

void DraggableInstructionElement::setInstruction(Instruction *instruction)
{
    _instruction = instruction; // set instruction
    _instruction->setParent(this);  // set this as parent of instruction
}

void DraggableInstructionElement::mousePressEvent(QMouseEvent * event) // mouse pressed
{
    if(_instruction != NULL)    // if instruction set
    {
        if (event->button() == Qt::LeftButton
                && event->pos().x() > 0 && event->pos().y() > 0 && event->pos().x() < width() && event->pos().y() < height())
        {   // if it is left button of mouse and pointer's position is above current element

                // prepare drag
                QDrag *drag = new QDrag(this);
                InstructionMimeData *mimeData = new InstructionMimeData();
                // fill mime data
                mimeData->setImageData(_instruction->image());
                mimeData->setText(_instruction->opcode());
                mimeData->setInstruction(_instruction);
                // set mime data and icon near mouse pointer
                drag->setMimeData(mimeData);
                drag->setPixmap(_instruction->image());
                // start drag
                drag->exec();
        }
    }
}

void DraggableInstructionElement::paintEvent(QPaintEvent *)
{
    if(_instruction != NULL) // if instructon set
    {
        QPainter painter(this);
        // draw instruction's icon
        painter.drawPixmap(width()/2 - _instruction->image().width()/2, height()/2 - _instruction->image().height()/2, _instruction->image().width(), _instruction->image().height(), _instruction->image());
    }
}
