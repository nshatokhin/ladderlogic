#include "componentpropertiesdialog.h"
#include "ui_componentpropertiesdialog.h"

ComponentPropertiesDialog::ComponentPropertiesDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ComponentPropertiesDialog)
{
    ui->setupUi(this);
}

ComponentPropertiesDialog::~ComponentPropertiesDialog()
{
    delete ui;
}

void ComponentPropertiesDialog::setElement(Element *element)
{
    _element = element; // save element for dialog

    ui->aliasField->setText(_element->instruction()->alias());  // fill alias field

    // save fields visibility for current element's type and fill it
    if(_element->instruction()->type() == Instruction::BASIC)
    {
            ui->dataSrc1Label->setText(tr("Data source"));
            ui->dataSrc1Label->setVisible(true);
            ui->dataSrc1Box->setVisible(true);
            ui->dataSrc2Label->setVisible(false);
            ui->dataSrc2Box->setVisible(false);
            ui->outputLabel->setVisible(false);
            ui->outputBox->setVisible(false);
            ui->timeValueLabel->setVisible(false);
            ui->timeValueBox->setVisible(false);


            BasicInstruction * basicInstruction = (BasicInstruction *) _element->instruction();
            int index = ui->dataSrc1Box->findText(basicInstruction->dataSrc());
            if(index >= 0)
                ui->dataSrc1Box->setCurrentIndex(index);
    }
    else if(_element->instruction()->type() == Instruction::TIMER)
    {
            ui->dataSrc1Label->setVisible(false);
            ui->dataSrc1Box->setVisible(false);
            ui->dataSrc2Label->setVisible(false);
            ui->dataSrc2Box->setVisible(false);
            ui->outputLabel->setVisible(false);
            ui->outputBox->setVisible(false);
            ui->timeValueLabel->setVisible(true);
            ui->timeValueBox->setVisible(true);

            TimerInstruction * timerInstruction = (TimerInstruction *) _element->instruction();
            int index = ui->timeValueBox->findText(timerInstruction->timeValue());
            if(index >= 0)
                ui->timeValueBox->setCurrentIndex(index);
    }
    else
    {
            ui->dataSrc1Label->setText(tr("Data source 1"));
            ui->dataSrc1Label->setVisible(true);
            ui->dataSrc1Box->setVisible(true);
            ui->dataSrc2Label->setVisible(true);
            ui->dataSrc2Box->setVisible(true);
            ui->outputLabel->setVisible(true);
            ui->outputBox->setVisible(true);
            ui->timeValueLabel->setVisible(false);
            ui->timeValueBox->setVisible(false);

            if(_element->instruction()->type() == Instruction::COMPARATOR)
            {
                ComparatorInstruction * comparatorInstruction = (ComparatorInstruction *) _element->instruction();
                int index;
                index = ui->dataSrc1Box->findText(comparatorInstruction->dataSrc1());
                if(index >= 0)
                    ui->dataSrc1Box->setCurrentIndex(index);
                index = ui->dataSrc2Box->findText(comparatorInstruction->dataSrc2());
                if(index >= 0)
                    ui->dataSrc2Box->setCurrentIndex(index);
                index = ui->outputBox->findText(comparatorInstruction->output());
                if(index >= 0)
                    ui->outputBox->setCurrentIndex(index);
            }
            else if(_element->instruction()->type() == Instruction::MATH)
            {
                MathInstruction * mathInstruction = (MathInstruction *) _element->instruction();
                int index;
                index = ui->dataSrc1Box->findText(mathInstruction->dataSrc1());
                if(index >= 0)
                    ui->dataSrc1Box->setCurrentIndex(index);
                index = ui->dataSrc2Box->findText(mathInstruction->dataSrc2());
                if(index >= 0)
                    ui->dataSrc2Box->setCurrentIndex(index);
                index = ui->outputBox->findText(mathInstruction->output());
                if(index >= 0)
                    ui->outputBox->setCurrentIndex(index);
            }
    }
}

void ComponentPropertiesDialog::setDataAliases(QStringList aliases)
{
    _aliases = aliases; // save aliases for dialog

    // clear drop down fields and store aliases to it
    ui->dataSrc1Box->clear();
    ui->dataSrc1Box->addItems(_aliases);
    ui->dataSrc2Box->clear();
    ui->dataSrc2Box->addItems(_aliases);
    ui->outputBox->clear();
    ui->outputBox->addItems(_aliases);
    ui->timeValueBox->clear();
    ui->timeValueBox->addItems(_aliases);
}

void ComponentPropertiesDialog::accept()
{
    // dialog accepted
    // set alias for element
    _element->instruction()->setAlias(ui->aliasField->text().trimmed());

    // set type dependend properties for element
    if(_element->instruction()->type() == Instruction::BASIC)
    {
        BasicInstruction * instruction = (BasicInstruction *) _element->instruction();

        int index = ui->dataSrc1Box->currentIndex();
        if(index >= 0)
            instruction->setDataSrc(ui->dataSrc1Box->itemText(index));
    }
    else if(_element->instruction()->type() == Instruction::TIMER)
    {
        TimerInstruction * instruction = (TimerInstruction *) _element->instruction();

        int index = ui->timeValueBox->currentIndex();
        if(index >= 0)
            instruction->setTimeValue(ui->timeValueBox->itemText(index));
    }
    else if(_element->instruction()->type() == Instruction::COMPARATOR)
    {
        ComparatorInstruction * instruction = (ComparatorInstruction *) _element->instruction();

        int index;
        index = ui->dataSrc1Box->currentIndex();
        if(index >= 0)
            instruction->setDataSrc1(ui->dataSrc1Box->itemText(index));
        index = ui->dataSrc2Box->currentIndex();
        if(index >= 0)
            instruction->setDataSrc2(ui->dataSrc2Box->itemText(index));
        index = ui->outputBox->currentIndex();
        if(index >= 0)
            instruction->setOutput(ui->outputBox->itemText(index));
    }
    else if(_element->instruction()->type() == Instruction::MATH)
    {
        MathInstruction * instruction = (MathInstruction *) _element->instruction();

        int index;
        index = ui->dataSrc1Box->currentIndex();
        if(index >= 0)
            instruction->setDataSrc1(ui->dataSrc1Box->itemText(index));
        index = ui->dataSrc2Box->currentIndex();
        if(index >= 0)
            instruction->setDataSrc2(ui->dataSrc2Box->itemText(index));
        index = ui->outputBox->currentIndex();
        if(index >= 0)
            instruction->setOutput(ui->outputBox->itemText(index));
    }

    QDialog::accept();
}

void ComponentPropertiesDialog::reject() // dialog was rejected
{
    QDialog::reject();
}
