#include "rung.h"

Rung::Rung(int x, int y, int width, QObject *parent) :
    QObject(parent)
{
    rungHeight = DEFAULT_RUNG_HEIGHT;
    lineHeight = DEFAULT_LINE_HEIGHT;
    lineWidth = DEFAULT_LINE_WIDTH;
    rungVerticalPadding = DEFAULT_RUNG_VERTICAL_PADDING;

    move(x, y); // move rung to given position
    resize(width, lineHeight);  // set given size

    // add start nodes and line
    addNode(new Node(0, lineHeight/2, this));
    addNode(new Node(width-1, lineHeight/2, this));
    _lines.push_back(new Line(0, 0, width, lineHeight, getNode(0), getNode(1), NULL, NULL, 0, this));
    _defaultLine = 0;
    _movingNode = NULL;
}

Rung::Rung(QDataStream &stream)
{
    rungHeight = DEFAULT_RUNG_HEIGHT;
    lineHeight = DEFAULT_LINE_HEIGHT;
    lineWidth = DEFAULT_LINE_WIDTH;
    rungVerticalPadding = DEFAULT_RUNG_VERTICAL_PADDING;

    load(stream);   // load rung from stream
}

Rung::~Rung()
{
    for(int i=0;i<_nodes.size();i++)    // for each node in list
        delete _nodes[i];   // destroy node
    _nodes.clear();     // clear nodes list

    for(int i=0;i<_elements.size();i++) // for each element in list
        delete _elements[i];    // destroy element
    _elements.clear();  // clear elements list

    for(int i=0;i<_lines.size();i++)    // for each line in list
        delete _lines[i];   // destroy line
    _lines.clear();     // clear lines list
}

int Rung::x()
{
    return _x;
}

int Rung::y()
{
    return _y;
}

void Rung::move(int x, int y)
{
    _x = x;
    _y = y;
}

int Rung::width()
{
    return _width;
}

int Rung::height()
{
    return _height;
}

void Rung::resize(int width, int height)
{
    _width = width;
    _height = height;
}

int Rung::startNodeNumber()
{
    return _startNodeNumber;
}

void Rung::setStartNodeNumber(int number)
{
    _startNodeNumber = number;
}

int Rung::maximumNodeNumber()   // return maximum node number in rung
{
    if(_nodes.size() > 0)   // if rung have nodes, return maximum index
        return _nodes.size() - 1;
    else
        return 0;   // else return 0
}

bool Rung::contains(int x, int y)
{
    // if coordinates located between top left corner and bottom right corner
    return ((x >= _x) && (y >= _y) &&
            (x < _x + _width) && (y < _y + _height));
}

void Rung::mousePressed(int, int)
{
    /*for(int i=0;i<_lines.size();i++)
    {
        if(_lines[i]->contains(x, y))
        {
            _activeLine = _lines[i];
            _movingLineLeftBorder = false;
            _movingLineRightBorder = false;

            if(x >= _lines[i]->x() && y >= _lines[i]->y() && x < _lines[i]->x() + 5 && y < _lines[i]->y() + _lines[i]->height())
            {
                _movingLineLeftBorder = true;
            }
            else if(x >= _lines[i]->x() + _lines[i]->width()-5 && y >= _lines[i]->y() && x < _lines[i]->x() + + _lines[i]->width() && y < _lines[i]->y() + _lines[i]->height())
            {
                _movingLineRightBorder = true;
            }

            if(_movingLineLeftBorder)
                _movingNode = _nodes.takeAt(_activeLine->inputNode()->index());
            else if(_movingLineRightBorder)
                _movingNode = _nodes.takeAt(_lines[i]->outputNode()->index());

            break;
        }
    }*/
}

void Rung::mouseReleased(int, int)
{
    /*int line = _defaultLine;

    for(int i=0;i<_lines.size();i++)
    {
        if(_lines[i]->contains(x, y))
        {
            line = i;
            break;
        }
    }

    if(_movingNode != NULL)
    {
        _movingNode->move(x, _lines[line]->y() + _lines[line]->height()/2);
        _nodes.push_back(_movingNode);

        if(_movingLineLeftBorder)
        {
            _activeLine->setInputNode(getNode(_nodes.size() - 1));
        }
        else if (_movingLineRightBorder)
        {
            _activeLine->setOutputNode(getNode(_nodes.size() - 1));
        }

        _movingNode = NULL;
    }

    _movingLineLeftBorder = false;
    _movingLineRightBorder = false;*/
}

void Rung::mouseMoving(int, int)
{
    /*if(_movingLineLeftBorder)
    {
        _activeLine->resize(_activeLine->x() + _activeLine->width() - x, _activeLine->height());
        _activeLine->move(x, _activeLine->y());

    }
    else if(_movingLineRightBorder)
    {
        _activeLine->resize(x - _activeLine->x(), _activeLine->height());
    }*/
}

QList<Line *> Rung::lines()
{
    return _lines;
}

QList<Node *> Rung::nodes()
{
    return _nodes;
}

QList<Element *> Rung::elements()
{
    return _elements;
}

QString Rung::toXml()   // convert rung to xml
{
    QString xml;

    for(int i=0;i<_elements.size();i++) // for each element in rung
    {
        // write it's properties as XML
        xml.append("<component>\n");
        xml.append(QString("<opcode>%1</opcode>\n").arg(_elements[i]->instruction()->opcode()));
        xml.append(QString("<alias>%1</alias>\n").arg(_elements[i]->instruction()->alias()));
        xml.append(QString("<in_node>%1</in_node>\n").arg(_startNodeNumber + _elements[i]->inputNode()->index()));
        xml.append(QString("<out_node>%1</out_node>\n").arg(_startNodeNumber + _elements[i]->outputNode()->index()));

        Instruction::Type type = _elements[i]->instruction()->type();   // get type of element

        // write type depended properties
        if(type == Instruction::BASIC)
        {
            BasicInstruction *basicInstruction = (BasicInstruction *) _elements[i]->instruction();
            xml.append(QString("<data_src>%1</data_src>\n").arg(basicInstruction->dataSrc()));
        }
        else if(type == Instruction::TIMER)
        {
            TimerInstruction *timerInstruction = (TimerInstruction *) _elements[i]->instruction();
            xml.append(QString("<timer_value>%1</timer_value>\n").arg(timerInstruction->timeValue()));
        }
        else if(type == Instruction::COMPARATOR)
        {
            ComparatorInstruction *comparatorInstruction = (ComparatorInstruction *) _elements[i]->instruction();
            xml.append(QString("<data_src_1>%1</data_src_1>\n").arg(comparatorInstruction->dataSrc1()));
            xml.append(QString("<data_src_2>%1</data_src_2>\n").arg(comparatorInstruction->dataSrc2()));
            xml.append(QString("<output>%1</output>\n").arg(comparatorInstruction->output()));
        }
        else if(type == Instruction::MATH)
        {
            MathInstruction *mathInstruction = (MathInstruction *) _elements[i]->instruction();
            xml.append(QString("<data_src_1>%1</data_src_1>\n").arg(mathInstruction->dataSrc1()));
            xml.append(QString("<data_src_2>%1</data_src_2>\n").arg(mathInstruction->dataSrc2()));
            xml.append(QString("<output>%1</output>\n").arg(mathInstruction->output()));
        }

        xml.append("</component>\n");
    }

    return xml; // return resulting XML
}

void Rung::load(QDataStream &stream)    // load rung from stream
{
    int x, y, width, height;
    stream >> x >> y >> width >> height;    // load rung's position and size
    move(x, y); // move rung
    resize(width, height);  // resize rung

    int nodesCount;
    stream >> nodesCount;   // load nodes count of rung

    for(int i=0;i<nodesCount;i++)   // for each node
    {   // load this node
        int x, y;
        stream >> x >> y;
        addNode(new Node(x, y));
    }

    int linesCount;
    int inputNode, outputNode, parentLineLeftIndex, parentLineRightIndex, lineIndex, node;
    QList<QPair<int, int> > parentLines;

    stream >> linesCount;   // load lines count of rung

    for(int i=0;i<linesCount;i++)   // for each line
    {
        // load line's params
        stream >> x >> y >> width >> height >> inputNode >> outputNode >> parentLineLeftIndex >> parentLineRightIndex >> nodesCount;

        // store parent line indexes
        parentLines.push_back(QPair<int, int>(parentLineLeftIndex, parentLineRightIndex));

        // create line with loaded params
        lineIndex = _lines.size();
        _lines.push_back(new Line(x, y, width, height, getNode(inputNode), getNode(outputNode), NULL, NULL, lineIndex));

        for(int j=0;j<nodesCount;j++)   // for each node of line
        {
            stream >> node; // load node index

            _lines[lineIndex]->addNode(getNode(node));  // add node to line
        }
    }

    // convert parent line indexes to line pointers
    for(int i=0;i<linesCount;i++)   // for each line
    {
        // get parent lines indexes
        parentLineLeftIndex = parentLines[i].first;
        parentLineRightIndex = parentLines[i].second;

        // set left parent line
        if(parentLineLeftIndex >= 0)
            _lines[i]->setParentLineLeft(_lines[parentLineLeftIndex]);
        // set right parent line
        if(parentLineRightIndex >= 0)
            _lines[i]->setParentLineRight(_lines[parentLineRightIndex]);
    }

    int elementsCount;
    stream >> elementsCount;    // load elements count

    Instruction * instruction;

    for(int i=0;i<elementsCount;i++)
    {
        int x, y, width, height, inputNode, outputNode, line;
        QString opcode;
        stream >> x >> y >> width >> height >> inputNode >> outputNode >> line >> opcode;   // load element's params

        instruction = instructionByOpcode(opcode);  // create temporary instruction for element
        instruction->load(stream);  // load instruction from stream
        // create element with given params
        Element * element = new Element(x, y, width, height, getNode(inputNode), getNode(outputNode), line, instruction);
        // add element to list
        _elements.push_back(element);
        _nodes[inputNode]->addRightElement(element);    // connect element to input node
        _nodes[outputNode]->addLeftElement(element);    // connect element to output node
        _lines[line]->addElement(element);              // add element to line

        delete instruction;     // destroy temporary instruction
    }
}

void Rung::save(QDataStream &stream)    // save rung to stream
{
    stream << _x << _y << _width << _height;    // save rung's coordinates and size

    int nodesCount = _nodes.size();
    stream << nodesCount;       // save rung's nodes count

    for(int i=0;i<nodesCount;i++)   // for each node
    {
        stream << _nodes[i]->x() << _nodes[i]->y(); // save node coordinates
    }

    int linesCount = _lines.size();
    stream << linesCount;   // save lines count

    for(int i=0;i<linesCount;i++)   // for each line
    {
        // save line params
        stream << _lines[i]->x() << _lines[i]->y() << _lines[i]->width() << _lines[i]->height()
               << _lines[i]->inputNode()->index() << _lines[i]->outputNode()->index();

        // save line's left parent
        if(_lines[i]->parentLineLeft() != NULL)
            stream << _lines[i]->parentLineLeft()->index();
        else
            stream << -1;
        // save line's right parent
        if(_lines[i]->parentLineRight() != NULL)
            stream << _lines[i]->parentLineRight()->index();
        else
            stream << -1;

        int nodesCount = _lines[i]->nodes().size();
        stream << nodesCount;       // save nodes count of this line

        for(int j=0;j<nodesCount;j++)   // for each node of line
        {
            Node * node = _lines[i]->nodes()[j];

            stream << node->index();    // save node index
        }
    }

    stream << _elements.size();     // save elements coint

    for(int i=0;i<_elements.size();i++) // for each element
    {
        // save element's params
        stream << _elements[i]->x() << _elements[i]->y() << _elements[i]->width() << _elements[i]->height()
               << _elements[i]->inputNode()->index() << _elements[i]->outputNode()->index() << _elements[i]->line() << _elements[i]->instruction()->opcode();
        // save element's instruction
        _elements[i]->instruction()->save(stream);
    }
}

void Rung::addElement(int x, int y, int frameX, int frameY, int, int, Instruction * instruction)    // add element to rung
{
    for(int i=0;i<_lines.size();i++)    // for each line of rung try to detect line for adding element
    {
        if(_lines[i]->contains(x - _x + frameX, y - _y + frameY))   // if line contains drop coordinates (frameX and frameY is offsets of scroll)
        {
            int elementX = x - _x + frameX; //calculate element's X
            int elementY = _lines[i]->y() + _lines[i]->height()/2 - instruction->image().height()/2;    // calculate element's height
            // calculate element's width and height
            int elementWidth = instruction->image().width();
            int elementHeight = instruction->image().height();
            Node * inputNode = NULL;
            Node * outputNode = NULL;

            // try to calculate element's position in list
            int elementPosition = -1;
            for(int j=0;j<_elements.size();j++)
            {
                if(_elements[j]->x() > elementX)    // find nearest right element
                    elementPosition = j;        // and set it's position to current's element position (current element will move it)
            }

            if(elementPosition == -1)   // if line don't have nearest right element
                elementPosition = _elements.length();   // add element to end of list

            for(int j=0;j<_lines[i]->elements().size();j++) // for each element of line
            {
                Element * existingELement = _lines[i]->elements()[j];

                if(existingELement->contains(x - _x + frameX, y - _y + frameY)) // if element was dropped above existing
                {   // add element to parallel line
                    qDebug() << "addElement: position is busy";

                    if(existingELement->x() - existingELement->inputNode()->x() < elementWidth/2)   // if element has near input node
                        inputNode = existingELement->inputNode();   // use this node
                    else
                    {
                        // otherwise add new node as input
                        inputNode = getNode(addNode(new Node(existingELement->x(), _lines[i]->y() + _lines[i]->height()/2)));
                        _lines[i]->addNode(inputNode);

                        // change input node of existing element to new
                        existingELement->inputNode()->removeRightElement(existingELement);
                        existingELement->setInputNode(inputNode);
                        inputNode->addRightElement(existingELement);
                    }

                    if(existingELement->outputNode()->x() - (existingELement->x() + existingELement->width()) < elementWidth/2) // if element has near output node
                        outputNode = existingELement->outputNode(); // use this node
                    else
                    {
                        // otherwise add new
                        outputNode = getNode(addNode(new Node(existingELement->x() + existingELement->width(), _lines[i]->y() + _lines[i]->height()/2)));
                        _lines[i]->addNode(outputNode);

                        // change output node of existing element to new
                        existingELement->outputNode()->removeLeftElement(existingELement);
                        existingELement->setOutputNode(outputNode);
                        outputNode->addLeftElement(existingELement);
                    }

                    // add new line
                    Line * newLine = new Line(existingELement->x(), _lines[i]->y() + _lines[i]->height(), elementWidth, lineHeight, inputNode, outputNode, _lines[i], _lines[i], _lines.size());

                    // find new line's Y position
                    bool positionFound = false;
                    int lineY = newLine->y();
                    while(!positionFound)   // while position for line not found
                    {
                        positionFound = true;   // reset flag
                        for(int j=0;j<_lines.size();j++)    // for each line of rung
                        {
                            if(newLine->intersection(_lines[j]))    // if any line of rung intersects with new
                            {
                                qDebug() << "intersection with line:" << j;
                                positionFound = false;  // go to loop again
                                lineY += _lines[j]->height();   // move new line down
                                newLine->move(newLine->x(), lineY);
                                break;
                            }
                        }
                    }
                    _lines.push_back(newLine);  // add new line to lines list

                    if(newLine->y() + newLine->height() > _height)  // if height of rung was increased
                    {
                        _height = newLine->y() + newLine->height(); // calculate new height
                        emit resized();     // tell draw area to reposition rungs
                    }

                    int newLineNum = _lines.size() - 1; // index of new line
                    // create new element
                    Element * element = new Element(existingELement->x(), newLine->y() + newLine->height()/2 - instruction->image().height()/2, elementWidth, elementHeight, inputNode, outputNode, newLineNum, instruction);
                    _lines[newLineNum]->addElement(element);    // add element to line

                    _elements.insert(elementPosition, element); // insert element to list

                    // connect element with it's nodes
                    inputNode->addRightElement(element);
                    outputNode->addLeftElement(element);

                    return;
                }
            }
            // element was dropped at free position
            if(elementWidth >= _lines[i]->width())  // if width of element bigger than width of line
            {
                elementX = _lines[i]->x() + _lines[i]->width()/2 - elementWidth/2;  // center element
                // choose input and output node for element
                inputNode = _lines[i]->inputNode();
                outputNode = _lines[i]->outputNode();

                // create new element
                Element * element = new Element(elementX, elementY, elementWidth, elementHeight, inputNode, outputNode, i, instruction);
                _lines[i]->addElement(element); // add element to line

                _elements.insert(elementPosition, element); // insert element to list

                // connect element with nodes
                inputNode->addRightElement(element);
                outputNode->addLeftElement(element);

                return;
            }
            else
            {
                if((elementX + elementWidth) > (_lines[i]->x() + _lines[i]->width()))  // if element's right border not located on line
                    elementX = _lines[i]->x() + _lines[i]->width() - elementWidth;  // move element to left
            }

            Node * leftNode = _lines[i]->findLeftNode(x - _x + frameX, y - _y + frameY);    // find nearest left node for element
            Node * rightNode = _lines[i]->findRightNode(x - _x + frameX, y - _y + frameY);  // find nearest right node for element

            // if nodes exist
            if(rightNode != NULL)
            {
                if(leftNode != NULL)
                {
                    // find all elements between two nodes
                    QList<Element *> elementsOnSegment = _lines[i]->findElementsBetweenTwoPoints(leftNode->x(), leftNode->y(), rightNode->x(), rightNode->y());

                    // if there is no elements between current nodes
                    if(elementsOnSegment.isEmpty())
                    {
                        // set element's input and output nodes to given left and right
                        inputNode = leftNode;
                        outputNode = rightNode;

                        // if space between this nodes smaller than element's width
                        if(outputNode->x() - inputNode->x() <= instruction->image().width())
                        {
                            elementX = inputNode->x() + (outputNode->x() - inputNode->x())/2 - elementWidth/2;  // center element between nodes
                        }
                    }
                    else    // otherwise there is one element between given nodes
                    {
                        Element * neighbourElement = elementsOnSegment[0];  // take this element

                        if(neighbourElement->x() < x - _x + frameX) // if element located at left of new
                        {
                            // create input node for new element (it will be output node for existing)
                            inputNode = getNode(addNode(new Node(x - _x + frameX, _lines[i]->y() + _lines[i]->height()/2)));
                            // take output node for new element from existing
                            outputNode = neighbourElement->outputNode();
                            // set added node as output for existing element
                            neighbourElement->setOutputNode(inputNode);

                            // reconnect nodes
                            inputNode->setLeftElements(outputNode->leftElements());
                            outputNode->clearLeftElements();

                            // add node to line
                            _lines[i]->addNode(inputNode);
                        }
                        else    // if element located at right of new
                        {
                            // take input node for new element from existing
                            inputNode = neighbourElement->inputNode();
                            // add new output node for new element (it will be input node for existing)
                            outputNode = getNode(addNode(new Node(x + elementWidth - _x + frameX, _lines[i]->y() + _lines[i]->height()/2)));
                            // set new node as input for existing element
                            neighbourElement->setInputNode(outputNode);

                            // reconnect nodes
                            outputNode->setRightElements(inputNode->rightElements());
                            inputNode->clearRightElements();
                            // add node to line
                            _lines[i]->addNode(outputNode);
                        }
                    }

                    // create new element
                    Element * element = new Element(elementX, elementY, elementWidth, elementHeight, inputNode, outputNode, i, instruction);
                    _lines[i]->addElement(element); // add element to line

                    _elements.insert(elementPosition, element); // insert element to elements list

                    // connect new element with input and output node
                    inputNode->addRightElement(element);
                    outputNode->addLeftElement(element);
                }
                else
                {
                    qDebug() << "leftNode is NULL";
                }
            }
            else
            {
                qDebug() << "rightNode NULL";
            }
        }
    }
}

void Rung::addLine(int x, int y, int frameX, int frameY, int, int)  // add new line to rung
{
    Node * inputNode = NULL;
    Node * outputNode = NULL;

    Node * rightNode = NULL;

    for(int i=0;i<_lines.size();i++)    // for each line of rung
    {
        // try to detect parent line for new
        if(_lines[i]->contains(x - _x + frameX, y - _y + frameY))   // if coordinates located at this line
        {
            rightNode = _lines[i]->findRightNode(x, y); // find nearest right node at this line

            if(rightNode != NULL)   // if right node exist
            {
                for(int j=0;j<rightNode->leftElements().size();j++) // for each element located at left of this node
                {
                    Element * leftElement = rightNode->leftElements()[j];   // get this element

                    if(_lines[i]->elements().contains(leftElement)) // if this element belongs to given line
                    {
                        if(leftElement->x() + leftElement->width() >= x)    // and it's right border located righter of given coordinates
                        {
                            rightNode = getNode(rightNode->index() - 1);    // get node from left of it
                        }

                        break;
                    }
                }

                int width = lineWidth;
                if(width >= _lines[i]->width()) // if new line's width bigger than parent line's width
                {
                    x = _lines[i]->x(); // move line to start of parent line
                    width = _lines[i]->width(); // set new line's width to parent line's width
                    // set input and output node the same as at parent line
                    inputNode = _lines[i]->inputNode();
                    outputNode = _lines[i]->outputNode();
                }
                else
                {
                    if((x + lineWidth) > (_lines[i]->x() + _lines[i]->width())) // if lines right border not located at parent line
                        x = _lines[i]->x() + _lines[i]->width() - lineWidth;    // move new line to left

                    // add input and output nodes for new line
                    inputNode = addNodeToLine(x, _lines[i]);
                    outputNode = addNodeToLine(x + lineWidth, _lines[i]);

                    // add new nodes to parent line
                    _lines[i]->addNode(inputNode);
                    _lines[i]->addNode(outputNode);
                }

                // create new line
                Line * newLine = new Line(x, _lines[i]->y() + _lines[i]->height(), width, lineHeight, inputNode, outputNode, _lines[i], _lines[i], _lines.size());

                // find new line's Y position
                bool positionFound = false;
                int lineY = newLine->y();
                while(!positionFound)   // if position for line not found
                {
                    positionFound = true;   // reset flag
                    for(int j=0;j<_lines.size();j++)    // for each line of rung
                    {
                        if(newLine->intersection(_lines[j])) // if any line of rung intersects with new
                        {
                            qDebug() << "intersection with line:" << j;
                            positionFound = false;  // go to loop again
                            lineY += _lines[j]->height();   // move new line down
                            newLine->move(newLine->x(), lineY);
                            break;
                        }
                    }
                }

                _lines.push_back(newLine);  // add line to lines list

                if(newLine->y() + newLine->height() > _height)  // if height of rung was increased
                {
                    _height = newLine->y() + newLine->height(); // calculate new height
                    emit resized(); // tell draw area to reposition rungs
                }
            }

            break;
        }
    }
}

void Rung::draw(QPainter &painter, int frameX, int frameY, int, int)    // draw lines and elements
{
    painter.fillRect(_x, _y, _x + _width, _x + _height, Qt::white); // draw white rectangle as rung's background

    for(int i=0;i<_lines.size();i++)    // for each line of rung
    {
        // draw this line
        painter.drawLine(_x + _lines[i]->x() - frameX, _y + _lines[i]->y()+_lines[i]->height()/2 - frameY, _x + _lines[i]->x()+_lines[i]->width() - frameX, _y + _lines[i]->y()+_lines[i]->height()/2 - frameY);

        // if line has a parents, draw connections to parent lines
        if(_lines[i]->parentLineLeft() != NULL)
            painter.drawLine(_x + _lines[i]->x() - 1 - frameX, _y + _lines[i]->parentLineLeft()->y() + _lines[i]->parentLineLeft()->height()/2 - frameY, _x + _lines[i]->x() - frameX, _y + _lines[i]->y()+_lines[i]->height()/2 - frameY);
        if(_lines[i]->parentLineRight() != NULL)
            painter.drawLine(_x + _lines[i]->x()+_lines[i]->width() + 1 - frameX, _y + _lines[i]->parentLineRight()->y() + _lines[i]->parentLineRight()->height()/2 - frameY, _x + _lines[i]->x()+_lines[i]->width() - frameX, _y + _lines[i]->y()+_lines[i]->height()/2 - frameY);
    }

    for(int i=0;i<_elements.size();i++) // for each element of rung
    {
        // draw image of this element
        painter.drawPixmap(_x + _elements[i]->x() - frameX, _y + _elements[i]->y() - frameY, _elements[i]->width(), _elements[i]->height(), _elements[i]->instruction()->image());
        // draw alias text of element
        painter.drawText(_x + _elements[i]->x() - frameX, _y + _elements[i]->y() - frameY, _elements[i]->instruction()->alias());
    }
}

void Rung::drawNodes(QPainter &painter, int frameX, int frameY, int, int)   // draw nodes of rung
{
    for(int i=0;i<_nodes.size();i++)    // for each node
    {
        // draw it
        painter.fillRect(_x + _nodes[i]->x()-5 - frameX, _y + _nodes[i]->y()-5 - frameY, 10, 10, Qt::green);
    }
}

int Rung::addNode(Node * node)  // add node to rung
{
    int index = 0;
    bool inserted = false;

    for(int i=0;i<_nodes.size();i++)    // for each node
    {
        // try to find righter node
        if(_nodes[i]->x() >= node->x()) // if it find
        {
            node->setIndex(i);  // set new node's index to current
            _nodes.insert(i,node);  // insert node to list
            index = i;
            inserted = true;    // node was inserted

            break;
        }
    }

    if(inserted)    // if node was inserted
    {
        //recalculate nodes indexes
        for(int i=index+1;i<_nodes.size();i++)
            _nodes[i]->setIndex(i);
    }
    else    // else
    {
        // add node to end of list
        _nodes.push_back(node);
        index = _nodes.length() - 1;
        node->setIndex(index);
    }

    emit nodeAdded();   //signal about adding node

    return index;
}

Node *Rung::addNodeToLine(int x, Line *line)
{
    Node * node = NULL;
    Node * neighbourNode;
    Node * leftNode = line->findLeftNode(x, line->y() + line->height()/2);  // find nearest left node
    Node * rightNode = line->findRightNode(x, line->y() + line->height()/2);    // findnearest right node

    // if left and right nodes exist
    if(rightNode != NULL)
    {
        if(leftNode != NULL)
        {
            // get elements between given nodes
            QList<Element *> elementsOnSegment = line->findElementsBetweenTwoPoints(leftNode->x(), leftNode->y(), rightNode->x(), rightNode->y());
            // if there is no elements between given nodes
            if(elementsOnSegment.isEmpty())
            {
                // add new node and add it to line
                node = getNode(addNode(new Node(x, line->y() + line->height()/2)));
                line->addNode(node);
            }
            else    // found one element between given nodes
            {
                // get it
                Element * neighbourElement = elementsOnSegment[0];
                // if it is at left of new node'sX
                if(neighbourElement->x() < x)
                {
                    // add new node
                    node = getNode(addNode(new Node(x, line->y() + line->height()/2)));
                    // get existing element's output node
                    neighbourNode = neighbourElement->outputNode();
                    // set new node as existing element's output node
                    neighbourElement->setOutputNode(node);

                    // reconnect nodes
                    node->setLeftElements(neighbourNode->leftElements());
                    neighbourNode->clearLeftElements();
                    // add node to line
                    line->addNode(node);
                }
                else    // if it is at right of new node's X
                {
                    // get existing element's input node
                    neighbourNode = neighbourElement->inputNode();
                    // add new node
                    node = getNode(addNode(new Node(x + neighbourElement->width(), line->y() + line->height()/2)));
                    // set new node as existing element's input node
                    neighbourElement->setInputNode(node);

                    // reconnect nodes
                    node->setRightElements(neighbourNode->rightElements());
                    neighbourNode->clearRightElements();

                    // add node to line
                    line->addNode(node);
                }
            }
        }
    }

    return node;    // return added node
}

Node *Rung::takeNode(int node)
{
    return _nodes.takeAt(node); // get node and remove it from list
}

Node *Rung::getNode(int node)
{
    return _nodes[node];    // get node without removing it from list
}

int Rung::findRightNode(int x, int, QList<Node *> nodes)
{
    for(int i=0;i<nodes.size();i++)
    {
        if(x < nodes[i]->x())   // if nearest node found
            return i;   // return it's index
    }

    return -1;  // else return -1
}

int Rung::findRightElement(int x, int, QList<Element *> &elements)
{
    for(int i=0;i<elements.size();i++)
    {
        if(x < elements[i]->x())    // if neares right element found
            return i;   // return it's index
    }

    return -1;  // else return -1
}

void Rung::reconnectNodes()
{
}
