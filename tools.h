#ifndef TOOLS_H
#define TOOLS_H

#include "instruction.h"
#include "basicinstruction.h"
#include "comparatorinstruction.h"
#include "mathinstruction.h"
#include "timerinstruction.h"

// contructs instruction by it's opcode and return pointer to it
Instruction * instructionByOpcode(QString opcode);

#endif // TOOLS_H
