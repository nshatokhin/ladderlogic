#include "instruction.h"

Instruction::Instruction(QObject *parent) :
    QObject(parent)
{
}

Instruction::Instruction(QString opcode, QString alias, QString imagePath, QObject *parent) :
    QObject(parent)
{
    _opcode = opcode;
    _alias = alias;
    _imagePath = imagePath;
    _image = QPixmap(imagePath);
}

Instruction::~Instruction()
{
}

Instruction::Type Instruction::type() const
{
    return _type;
}

QString Instruction::opcode() const
{
    return _opcode;
}

QString Instruction::alias() const
{
    return _alias;
}

QPixmap Instruction::image() const
{
    return _image;
}

QString Instruction::imagePath() const
{
    return _imagePath;
}

void Instruction::setOpcode(QString opcode)
{
    _opcode = opcode;
}

void Instruction::setAlias(QString alias)
{
    _alias = alias;
}

void Instruction::setImage(QPixmap image)
{
    _image = image;
}

void Instruction::setImagePath(QString imagePath)
{
    _imagePath = imagePath; // set imagePath

    _image = QPixmap(imagePath);    // and load image by it
}

void Instruction::save(QDataStream &stream)
{
    // save instruction params to stream
    stream << _type;
    stream << _opcode;
    stream << _alias;
    stream << _imagePath;
}

void Instruction::load(QDataStream &stream)
{
    // load instruction params from stream
    quint32 type;
    stream >> type;
    _type = (Type) type;
    stream >> _opcode;
    stream >> _alias;
    stream >> _imagePath;
    _image = QPixmap(_imagePath);
}
