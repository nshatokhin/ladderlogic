#ifndef MATHINSTRUCTION_H
#define MATHINSTRUCTION_H

#include "instruction.h"

class MathInstruction : public Instruction
{
    Q_OBJECT
public:
    explicit MathInstruction(QString opcode, QString alias, QString dataSrc1, QString dataSrc2, QString output, QString imagePath, QObject *parent = 0);

    // setters & getters of instruction properties
    QString dataSrc1();
    void setDataSrc1(QString dataSrc);
    QString dataSrc2();
    void setDataSrc2(QString dataSrc);
    QString output();
    void setOutput(QString output);

    // create object's copy
    Instruction * clone();

    // save & load function
    void save(QDataStream &stream);
    void load(QDataStream &stream);

signals:

public slots:

protected:
    // instruction properties
    QString _dataSrc1;
    QString _dataSrc2;
    QString _output;
};

#endif // MATHINSTRUCTION_H
