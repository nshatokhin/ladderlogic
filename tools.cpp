#include "tools.h"

Instruction * instructionByOpcode(QString opcode)   // return instruction by it's opcode
{
    if(opcode == "XIC")
        return new BasicInstruction("XIC", "", "", ":/assets/instructions/basic/XIC.png");
    else if(opcode == "XIO")
        return new BasicInstruction("XIO", "", "", ":/assets/instructions/basic/XIO.png");
    else if(opcode == "OTE")
        return new BasicInstruction("OTE", "", "", ":/assets/instructions/basic/OTE.png");
    else if(opcode == "OTL")
        return new BasicInstruction("OTL", "", "", ":/assets/instructions/basic/OTL.png");
    else if(opcode == "OTU")
        return new BasicInstruction("OTU", "", "", ":/assets/instructions/basic/OTU.png");
    else if(opcode == "OSR")
        return new BasicInstruction("OSR", "", "", ":/assets/instructions/basic/OSR.png");
    else if(opcode == "TON")
        return new TimerInstruction("TON", "", "", ":/assets/instructions/timers/TON.png");
    else if(opcode == "TOF")
        return new TimerInstruction("TOF", "", "", ":/assets/instructions/timers/TOF.png");
    else if(opcode == "CTU")
        return new TimerInstruction("CTU", "", "", ":/assets/instructions/timers/CTU.png");
    else if(opcode == "CTD")
        return new TimerInstruction("CTD", "", "", ":/assets/instructions/timers/CTD.png");
    else if(opcode == "RES")
        return new TimerInstruction("RES", "", "", ":/assets/instructions/timers/RES.png");
    else if(opcode == "EQU")
        return new ComparatorInstruction("EQU", "", "", "", "", ":/assets/instructions/comparators/EQU.png");
    else if(opcode == "NEQ")
        return new ComparatorInstruction("NEQ", "", "", "", "", ":/assets/instructions/comparators/NEQ.png");
    else if(opcode == "LES")
        return new ComparatorInstruction("LES", "", "", "", "", ":/assets/instructions/comparators/LES.png");
    else if(opcode == "LEQ")
        return new ComparatorInstruction("LEQ", "", "", "", "", ":/assets/instructions/comparators/LEQ.png");
    else if(opcode == "GRT")
        return new ComparatorInstruction("GRT", "", "", "", "", ":/assets/instructions/comparators/GRT.png");
    else if(opcode == "GEQ")
        return new ComparatorInstruction("GEQ", "", "", "", "", ":/assets/instructions/comparators/GEQ.png");
    else if(opcode == "MEQ")
        return new ComparatorInstruction("MEQ", "", "", "", "", ":/assets/instructions/comparators/MEQ.png");
    else if(opcode == "LIM")
        return new ComparatorInstruction("LIM", "", "", "", "", ":/assets/instructions/comparators/LIM.png");
    else if(opcode == "ADD")
        return new MathInstruction("ADD", "", "", "", "", ":/assets/instructions/math/ADD.png");
    else if(opcode == "SUB")
        return new MathInstruction("SUB", "", "", "", "", ":/assets/instructions/math/SUB.png");
    else if(opcode == "MUL")
        return new MathInstruction("MUL", "", "", "", "", ":/assets/instructions/math/MUL.png");
    else if(opcode == "DIV")
        return new MathInstruction("DIV", "", "", "", "", ":/assets/instructions/math/DIV.png");
    else
        return NULL;
}
